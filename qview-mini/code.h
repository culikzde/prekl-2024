
/* code.h */

#ifndef CODE_H
#define CODE_H

/* ---------------------------------------------------------------------- */

#include <string>
#include <vector>
#include <unordered_map>

using std::string;
using std::vector;
using std::unordered_map;

/* ---------------------------------------------------------------------- */

namespace llvm
{
    class Value;
    class Instruction;
}

namespace clang
{
    class Decl;
}

typedef llvm::Value LlvmValue;
typedef clang::Decl ClangDecl;

/* ---------------------------------------------------------------------- */

class CmmExpr;
class CmmDecl;

/* ---------------------------------------------------------------------- */

class SimScope;
class SimNamespace;
class SimClass;
class SimEnum;
class SimVariable;
class SimFunction;

class SimBasic
{
public:
    string name;

    int src_file = 0;
    int src_line = 0;
    int src_col  = 0;

    CmmDecl * cmm_decl = nullptr;
    clang::Decl * clang_decl = nullptr;

    virtual SimScope * conv_SimScope () { return nullptr; }
    virtual SimNamespace * conv_SimNamespace () { return nullptr; }
    virtual SimClass * conv_SimClass () { return nullptr; }
    virtual SimEnum * conv_SimEnum () { return nullptr; }
    virtual SimFunction * conv_SimFunction () { return nullptr; }
    virtual SimVariable * conv_SimVariable () { return nullptr; }
};

/* ---------------------------------------------------------------------- */

class SimScope : public SimBasic
{
public:
    unordered_map <string, SimBasic *> item_dict;
    vector <SimBasic *> item_list;
    vector <SimScope *> using_list;

    SimBasic * get (string name)
    {
        SimBasic * result = nullptr;
        if (item_dict.find (name) != item_dict.end ())
           result = item_dict [name];
        return result;
    }

    void add (SimBasic * item)
    {
        item_list.push_back (item);
        if (item->name != "")
           item_dict [item->name] = item;
    }

    SimScope * conv_SimScope () override { return this; }
};

/* ---------------------------------------------------------------------- */

class SimNamespace : public SimScope
{
public:
    SimNamespace * conv_SimNamespace () override { return this; }
};

class SimClass : public SimScope
{
public:
    SimClass * conv_SimClass () override { return this; }
};

class SimEnum : public SimScope
{
public:
    SimEnum * conv_SimEnum () override { return this; }
};

class SimVariable : public SimBasic
{
public:
    SimVariable * conv_SimVariable () override { return this; }
};

class SimFunction : public SimScope
{
public:
    vector <SimVariable *> parameters;
    SimFunction * conv_SimFunction () override { return this; }
};

/* --------------------------------------------------------------------- */

class SimSimpleType;
class SimNamedType;
class SimCompoundType;
class SimPointerType;
class SimReferenceType;
class SimArrayType;
class SimFunctionType;

class SimType
{
public:
    bool type_const;
    bool type_volatile;


    SimType () :
       type_const (false),
       type_volatile (false)
    { }

    virtual SimType * from () { return nullptr; }
    virtual SimSimpleType * conv_SimSimpleType () { return nullptr; }
    virtual SimNamedType * conv_SimNamedType () { return nullptr; }
    virtual SimCompoundType * conv_SimCompoundType () { return nullptr; }
    virtual SimPointerType * conv_SimPointerType () { return nullptr; }
    virtual SimReferenceType * conv_SimReferenceType () { return nullptr; }
    virtual SimArrayType * conv_SimArrayType () { return nullptr; }
    virtual SimFunctionType * conv_SimFunctionType () { return nullptr; }
};

class SimSimpleType : public SimType
{
public:
    bool type_signed;
    bool type_unsigned;

    bool type_void;
    bool type_bool;
    bool type_char;
    bool type_wchar;

    bool type_short;
    bool type_int;
    bool type_long;

    bool type_float;
    bool type_double;

    SimSimpleType () :
       type_signed (false),
       type_unsigned (false),

       type_void (false),
       type_bool (false),
       type_char (false),
       type_wchar (false),

       type_short (false),
       type_int (false),
       type_long (false),

       type_float (false),
       type_double (false)
       { }

    SimSimpleType * conv_SimSimpleType () override { return this; }
};

class SimNamedType : public SimType
{
public:
    string type_name;
    CmmDecl * type_decl;

    SimNamedType () : type_decl (nullptr) { }
    SimNamedType * conv_SimNamedType () override { return this; }
};

class SimCompoundType : public SimType
{
public:
    SimType * type_from ;
    SimCompoundType () : type_from (nullptr) { }

    SimType * from () override { return type_from; }
    SimCompoundType * conv_SimCompoundType () override { return this; }
};

class SimPointerType : public SimCompoundType
{
    SimPointerType * conv_SimPointerType () override { return this; }
};

class SimReferenceType : public SimCompoundType
{
    SimReferenceType * conv_SimReferenceType () override { return this; }
};

class SimArrayType : public SimCompoundType
{
public:
    CmmExpr * type_lim;
    SimArrayType () : type_lim (nullptr) { }
    SimArrayType * conv_SimArrayType () override { return this; }
};

class SimFunctionType : public SimCompoundType
{
public:
    vector <SimType *> parameter_types;
    SimFunctionType * conv_SimFunctionType () override { return this; }
};

/* ---------------------------------------------------------------------- */

class DisplayStack
{
private :
      vector <SimScope *> list;
   public:
      int size () { return list.size (); }
      SimScope * get (int inx) { return list [inx]; }
      SimScope * top () { return list [list.size () - 1]; }

      void push (SimScope * value) { list.push_back (value); }
      void pop () { list.resize (list.size () - 1); }
};

/* ---------------------------------------------------------------------- */

#endif // CODE_H
