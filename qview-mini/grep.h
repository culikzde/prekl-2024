/* grep.h */

#ifndef GREP_H
#define GREP_H

#include <QTreeWidget>
#include <QPushButton>
#include <QCheckBox>
#include <QComboBox>
#include <QDialog>

#include <QProcess>

#include "tree.h"

class Window;

/* ---------------------------------------------------------------------- */

class GrepView: public QWidget
{
   public:
      GrepView (Window * p_win);

      void prevItem ();
      void nextItem ();
      void grep (QString params);

   public:
      void onStopButton ();
      void onStartProcess ();
      void onStopProcess ();
      void onDataReady ();

   private:
      Window * win;

      QProcess * process;

      QTreeWidget * tree;
      QPushButton * stopButton;

      QString lastFileName;
      TreeItem * branch;

      bool startLine;
      bool middle;
      QString fileName;
      int lineNum;
      QString text;

      void initVariables ();
      void initLineVariables ();

      void showItem ();
};

/* ---------------------------------------------------------------------- */

class GrepDialog: public QDialog
{
   public:
      GrepDialog (GrepView * p_view);

   public:
      void search ();

   private:
      GrepView * grep_view;

      QLineEdit * pattern;
      QCheckBox * caseSensitive;
      QCheckBox * wholeWords;
      QCheckBox * regularExpression;
      QComboBox * directory;
};

/* ---------------------------------------------------------------------- */

#endif // GREP_H
