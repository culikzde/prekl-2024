#ifndef PROPERTY_H
#define PROPERTY_H

#include <QTableWidget>
#if QT_VERSION >= 0x040400
   #include <QStyledItemDelegate>
#else
   #include <QItemDelegate>
#endif
#include <QGraphicsItem>

#include <QCheckBox>
#include <QComboBox>
#include <QLineEdit>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QPushButton>

// #include "win.h"

class PropertyTable : public QTableWidget
{
private:
    QTableWidgetItem * addTableLine (QString name, QString value);
    void storeProperty (QGraphicsItem * item, QString name, QVariant value);

public:
    PropertyTable (QWidget * parent = NULL);
    void displayProperties (QGraphicsItem * item);
    void clearTable ();

    void addBool (QString name, bool value);
    void addInt (QString name, int value);
    void addDouble (QString name, double value);
    void addString (QString name, QString value);
    void addColor (QString name, QColor value);
    void addFont (QString name, QFont value);
    void addList (QString name, QStringList value);

public:
    // void onItemChanged (QTableWidgetItem * item);

private:
    // Win * win;
    QGraphicsItem * graphics_item;

public:
    // void setWin (Win * p_win) { win = p_win; }
};

class CustomEditor : public QWidget
{
public:
      bool enable_checkbox;
      bool enable_list;
      bool enable_text;
      bool enable_numeric;
      bool enable_real;
      bool enable_dialog;

      QStringList list_values;

      QCheckBox * check_box;
      QLineEdit * line_edit;
      QSpinBox * numeric_edit;
      QDoubleSpinBox * real_edit;
      QComboBox * combo_box;
      QPushButton * button;

      QTableWidgetItem * cell;

      void onDialogClick ();

      CustomEditor (QWidget * parent) :
            QWidget (parent),
            enable_checkbox (false),
            enable_list (false),
            enable_text (false),
            enable_numeric (false),
            enable_real (false),
            enable_dialog (false),

            check_box (NULL),
            line_edit (NULL),
            numeric_edit (NULL),
            real_edit (NULL),
            combo_box (NULL),
            button (NULL),

            cell (NULL)
      { }
};

class CustomDelegate : public
   #if QT_VERSION >= 0x040400
       QStyledItemDelegate
   #else
       QItemDelegate
   #endif
{
   public:
      virtual QWidget * createEditor
         (QWidget * parent,
          const QStyleOptionViewItem & option,
          const QModelIndex & index) const override;

      virtual void setEditorData
         (QWidget * param_editor,
          const QModelIndex & index) const override;

      virtual void setModelData
         (QWidget * param_editor,
          QAbstractItemModel * model,
          const QModelIndex & index) const override;

    /*
    virtual void updateEditorGeometry
       (QWidget * param_editor,
        const QStyleOptionViewItem & option,
        const QModelIndex &index) const override;

    virtual QSize sizeHint
       (const QStyleOptionViewItem & option,
        const QModelIndex & index) const override;
    */

    PropertyTable * table;
    CustomDelegate ( PropertyTable * p_table) : table (p_table) { }
};

#endif // PROPERTY_H
