
/* view.cc */

#include "view.h"

#ifdef USE_MINI
   // #include "mini-parser.hpp"
   #include "mini-product.hpp"
   #include "mini-comp.h"
#endif

#include "clang-util.h"
#include "clang-common.h"
#include "clang-jit.h"

#ifdef USE_LLDB
   #include "clang-debugger.h"
#endif

#ifdef USE_LLDB_TRACE
   #include "lldb/API/LLDB.h"
#endif

#include <QApplication>
#include <QFileDialog>
#include <QFileInfo>
#include <QMessageBox>

#include <QMenuBar>
#include <QMenu>
#include <QAction>
#include <QHeaderView>
#include <QStatusBar>

#include <QShortcut>
#include <QObject>

#include <QProcess>

#ifdef USE_INSTR
   #include "mini-instr.h"
#endif

#ifdef USE_TRACE
   #include "trace.h" // set_error_handlers
#endif

#include <iostream>
#include <string>
#include <iostream>
using namespace std;

/* ---------------------------------------------------------------------- */

void Window::addMenuItem (QMenu * menu, QString title, QObject * obj, const char * slot, const char * shortcut)
{
   QAction * act = new QAction (title, menu);
   if (shortcut != NULL)
      act->setShortcut (QKeySequence (shortcut));
   QObject::connect (act, SIGNAL (triggered ()), obj, slot);
   menu->addAction (act);
}

void Window::addView (QString title, QWidget * widget)
{
    int inx = tabs->addTab (widget, title);
    tabs->setCurrentIndex (inx);
}

/* ---------------------------------------------------------------------- */

#define ADD_SHORTCUT(func, shortcut) \
   { \
      QShortcut * act = new QShortcut (this);\
      act->setObjectName (#func); \
      act->setKey (QKeySequence (shortcut)); \
      act->setContext (Qt::ApplicationShortcut); \
      connect (act, SIGNAL (activated ()), this, SLOT (func ())); \
   }

//  QShortcut (QKeySequence (#shortcut), this, SLOT (#func ())); */
//  act->setShortcut (QKeySequence (shortcut));

#define ADD_FUNC(name) \
   void Window::name () \
   { \
      Edit * edit = getEditor (); \
      if (edit != NULL) \
         edit->name (); \
   }

#define ADD_CALL(name, call) \
   void Window::name () \
   { \
      Edit * edit = getEditor (); \
      if (edit != NULL) \
         edit->call (); \
   }

ADD_CALL (find, findText)
ADD_FUNC (findPrev)
ADD_FUNC (findNext)
ADD_CALL (replace, replaceText)
ADD_FUNC (findSelected)
ADD_FUNC (findIncremental)
ADD_FUNC (goToLine)
// ADD_FUNC (findInFiles)

ADD_FUNC (indent)
ADD_FUNC (unindent)

ADD_FUNC (comment)
ADD_FUNC (uncomment)

ADD_FUNC (setBookmark)
ADD_CALL (prevBookmark, gotoPrevBookmark)
ADD_CALL (nextBookmark, gotoNextBookmark)
ADD_FUNC (clearBookmarks)

ADD_FUNC (enlargeFont)
ADD_FUNC (shrinkFont)

ADD_CALL (prevFunction, gotoPrevFunction)
ADD_CALL (nextFunction, gotoNextFunction)

ADD_FUNC (moveLinesUp)
ADD_FUNC (moveLinesDown)

ADD_FUNC (saveFile)
ADD_FUNC (saveFileAs)
ADD_FUNC (reloadFile)

/* ---------------------------------------------------------------------- */

Window::Window (QWidget * parent) :
   QMainWindow (parent)
{
   /* File Menu */

   fileMenu = menuBar()->addMenu ("&File");
   addMenuItem (fileMenu, "&Open ...", this, SLOT (openFile ()), "Ctrl+O");
   addMenuItem (fileMenu, "&Save", this, SLOT (saveFile ()), "Ctrl+S");
   addMenuItem (fileMenu, "Save &As ...", this, SLOT (saveFileAs ()), "Ctrl+Shift+S");
   addMenuItem (fileMenu, "&Reload", this, SLOT (reloadFile ()));
   fileMenu->addSeparator ();
   addMenuItem (fileMenu, "&Quit", this, SLOT (quit ()), "Ctrl+Q");

   /* Edit Menu */

   editMenu = menuBar()->addMenu ("&Edit");
   addMenuItem (editMenu, "&Indent", this, SLOT (indent ()), "Ctrl+I");
   addMenuItem (editMenu, "&Unindent", this, SLOT (unindent ()), "Ctrl+U");
   editMenu->addSeparator ();
   addMenuItem (editMenu, "Comment", this, SLOT (comment ()), "Ctrl+D");
   addMenuItem (editMenu, "Uncomment", this, SLOT (uncomment ()), "Ctrl+E");
   editMenu->addSeparator ();
   addMenuItem (editMenu, "&Find", this, SLOT (find ()), "Ctrl+F");
   addMenuItem (editMenu, "&Replace", this, SLOT (replace ()), "Ctrl+R");
   addMenuItem (editMenu, "Find &Next", this, SLOT (findNext ()), "F3");
   addMenuItem (editMenu, "Find &Previous", this, SLOT (findPrev ()), "F3");
   addMenuItem (editMenu, "&Grep", this, SLOT (findInFiles ()), "F2");
   editMenu->addSeparator ();
   addMenuItem (editMenu, "Set Bookmark", this, SLOT (setBookmark ()), "Ctrl+B");
   addMenuItem (editMenu, "Previous Bookmark", this, SLOT (prevBookmark ()), "Alt+PgUp");
   addMenuItem (editMenu, "Next Bookmark", this, SLOT (nextBookmark ()), "Alt+PgDown");
   addMenuItem (editMenu, "Clear All Bookmarks", this, SLOT (clearBookmarks ()));
   editMenu->addSeparator ();
   addMenuItem (editMenu, "Previous Function", this, SLOT (prevFunction ()), "Alt+Up");
   addMenuItem (editMenu, "Next Function", this, SLOT (nextFunction ()), "Alt+Down");
   editMenu->addSeparator ();
   addMenuItem (editMenu, "Move Lines Up", this, SLOT (moveLinesUp ()), "Ctrl+Shift+Up");
   addMenuItem (editMenu, "Move Lines Down", this, SLOT (moveLinesDown ()), "Ctrl+Shift+Down");
   // editMenu->addSeparator ();
   // addMenuItem (editMenu, "Auto Completion", this, SLOT (complete ()), "Ctrl+Space");


   /* View Menu */

   viewMenu = menuBar()->addMenu ("&View");

   addMenuItem (viewMenu, "Enlarge Font", this, SLOT(enlargeFont ()), "Ctrl++");
   addMenuItem (viewMenu, "Shrint Font", this, SLOT(shrinkFont ()), "Ctrl+-");
   viewMenu->addSeparator ();

   #ifdef USE_DESIGNER
      addMenuItem (viewMenu, "Qt &Designer", this, SLOT(showDesigner ()));
   #endif

   /* Run Menu */

   runMenu = menuBar()->addMenu ("&Run");

   #ifdef USE_MINI
      addMenuItem (runMenu, "&Run Mini", this, SLOT (runMini ()), "F9");
      // runMenu->addSeparator ();
   #endif

   #ifdef USE_CLANG
      addMenuItem (runMenu, "&Declarations (from C++ clang)", this, SLOT(showClangDecl ())), "F10";

      addMenuItem (runMenu, "&Interpreter (clang JIT)", this, SLOT (runInterpreter ()), "F11");
   #endif

   addMenuItem (runMenu, "&Compiler (command line)", this, SLOT (runCompiler ()));

   #ifdef USE_LLDB
      addMenuItem (runMenu, "&Debugger", this, SLOT (runDebugger ()));
   #endif

   /* layout */

   left_tools = new QTabWidget (this);
   left_tools->setTabPosition (QTabWidget::West);

   right_tools = new QTabWidget (this);
   right_tools->setTabPosition (QTabWidget::East);

   tabs = new QTabWidget (this);
   info = new QTextEdit (this);

   find_box = new FindBox (this);
   line_box = new GoToLineBox (this);

   middle_widget = new QWidget (this);
   middle_layout = new QVBoxLayout (middle_widget);
   middle_layout->addWidget (tabs);
   middle_layout->addWidget (find_box);
   middle_layout->addWidget (line_box);

   hsplitter = new QSplitter (this);
   hsplitter->addWidget (left_tools);
   hsplitter->addWidget (middle_widget);
   hsplitter->addWidget (right_tools);

   vsplitter = new QSplitter (this);
   vsplitter->setOrientation (Qt::Vertical);
   vsplitter->addWidget (hsplitter);
   vsplitter->addWidget (info);

   setCentralWidget (vsplitter);

   hsplitter->setStretchFactor (0, 1);
   hsplitter->setStretchFactor (1, 4);

   vsplitter->setStretchFactor (0, 4);
   vsplitter->setStretchFactor (1, 1);

   /* left tools */

   tree = new Tree (this);
   project = new Tree (this);
   classes = new Tree (this);

   left_tools->addTab (tree, "Tree");
   left_tools->addTab (classes, "Classes");
   left_tools->addTab (project, "Project");

   file_view = new FileView (this);
   left_tools->addTab (file_view, "Files");

   grep_view = new GrepView (this);
   left_tools->addTab (grep_view, "Grep");

   #if 0
       browser = new Browser;
       browser->tree = tree;
       browser->prop = prop;
   #endif

   /* right tools */

   prop = new PropertyTable (this);
   right_tools->addTab (prop, "Properties");

   /* editor tabs */

   empty_edit = NULL;

   setMinimumSize (640, 480);
   statusBar()->showMessage (QString ("Qt ") + qVersion());
}

Window::~Window()
{
}

void Window::removeEmptyEditor ()
{
    int inx = tabs->indexOf (empty_edit);
    if (inx >= 0)
       tabs->removeTab (inx);
}

/* ---------------------------------------------------------------------- */

void Window::readFile (Edit * edit, QString fileName)
{
   QFile f (fileName);
   if (f.open (QFile::ReadOnly))
   {
      QByteArray code = f.readAll ();
      edit->setPlainText (code);
   }
   else
   {
      QMessageBox::warning (NULL, "Open File Error", "Cannot read file: " + fileName);
   }
}

Edit * Window::openEditor (QString fileName1, int line, int col)
{
   // cout << "openEditor " << fileName1.toStdString () << endl;

   Edit * edit = NULL;
   QFileInfo fi (fileName1);
   QString fileName2 = fi.absoluteFilePath();
   // string fileName3 = fileName2.toStdString ();

   if (sourceEditors.contains (fileName2))
   {
      edit = sourceEditors [fileName2];
      // edit = sourceEditors.get (fileName3);
   }
   else
   {
      edit = new Edit (this);
      edit->setToolTip (fileName2);
      edit->setFileName (fileName2);
      // edit->setProperty ("fileName", fileName);
      edit->findBox = find_box;
      edit->lineBox = line_box;
      tabs->addTab (edit, fi.fileName ());

      sourceEditors [fileName2] = edit;
      // sourceEditors.store (fileName3, edit);

      readFile (edit, fileName2);
   }
   tabs->setCurrentWidget (edit);
   if (line != 0)
   {
       // edit->select (line, col);
       edit->selectLine (line);
   }
   return edit;
}

void Window::openFile ()
{
   QString fileName = QFileDialog::getOpenFileName (this, "Open file");
   if (fileName != "")
      openEditor (fileName);
}

Edit * Window::getEditor ()
{
   Edit * edit = dynamic_cast < Edit * > (tabs->currentWidget ());
   return edit;
}

QString Window::getEditorFileName (Edit * edit)
{
   QString result = "";
   if (edit != NULL)
      result = edit->getFileName ();
      // result = edit->property ("fileName").toString ();
   return result;
}

QString Window::getCurrentFileName ()
{
   Edit * edit = getEditor ();
   return getEditorFileName (edit);
}

QString Window::getCurrentText ()
{
   QString result = "";
   Edit * edit = getEditor ();
   if (edit != NULL)
      result = edit->toPlainText ();
   return result;
}

void selectLine (Edit * edit, int line, int column = 0)
{
   if (edit != NULL && line > 0)
   {
       QTextCursor cursor = edit->textCursor ();

       cursor.movePosition (QTextCursor::Start);
       cursor.movePosition (QTextCursor::QTextCursor::Down, QTextCursor::MoveAnchor, line-1);

       if (column > 0)
       {
          cursor.movePosition (QTextCursor::QTextCursor::Right, QTextCursor::MoveAnchor, column-1);

          // select to the end of line
          cursor.movePosition (QTextCursor::QTextCursor::EndOfLine, QTextCursor::KeepAnchor);
       }
       else
       {
          // select line
          cursor.movePosition (QTextCursor::QTextCursor::Down, QTextCursor::KeepAnchor, 1);
       }

       edit->setTextCursor (cursor);
       edit->ensureCursorVisible ();
   }
}

/* ---------------------------------------------------------------------- */

void Window::findInFiles ()
{
   left_tools->setCurrentWidget (grep_view);
   new GrepDialog (grep_view);
}

/*
void Window::complete ()
{
    Edit * edit = getEditor ();
    if (edit != NULL)
       edit->showCompletion ();
}
*/

/* ---------------------------------------------------------------------- */

void Window::displayProperties (TreeItem * item)
{
}

/* ---------------------------------------------------------------------- */

#ifdef USE_MINI
void displayDeclarations (QTreeWidgetItem * target, SimScope * scope);

void displayDeclaration (QTreeWidgetItem * target, SimBasic * basic)
{
    TreeItem * node = new TreeItem;
    node->setText (0, QString::fromStdString (basic->name));
    node->src_file = basic->src_file;
    node->src_line = basic->src_line;
    node->src_col  = basic->src_col;

    node->sim_obj = basic;
    node->cmm_obj = basic->cmm_decl;
    #ifdef USE_CLANG
    node->clang_obj = basic->clang_decl;
    #endif

    target->addChild (node);

    SimScope * scope = basic->conv_SimScope ();
    if (scope != nullptr)
       displayDeclarations (node, scope);
}

void displayDeclarations (QTreeWidgetItem * target, SimScope * scope)
{
    if (scope != nullptr)
    {
        for (SimBasic * basic : scope->item_list)
        {
            displayDeclaration (target, basic);
        }
    }
}

#endif

void Window::runMini ()
{
    #ifdef USE_MINI
    QString fileName = getCurrentFileName ();
    if (fileName != "")
    {
       SimScope * scope = compile_program (fileName.toStdString ());

       classes->clear ();
       left_tools->setCurrentWidget (classes);
       displayDeclarations (classes->invisibleRootItem (), scope);

       #ifdef USE_INSTR
          instruction_test (scope);
       #endif

    }
    #endif
}

/* ---------------------------------------------------------------------- */

void Window::showClangDecl ()
{
    #ifdef USE_CLANG
       QString fileName = getCurrentFileName ();
       if (fileName != "")
       {
          left_tools->setCurrentWidget (tree);
          tree->clear ();
          displayTranslationUnit (tree, classes, fileName);
       }
    #endif
}

/* ---------------------------------------------------------------------- */

void Window::showDesigner ()
{
   #ifdef USE_DESIGNER
      QString fileName = getCurrentFileName ();
      if (fileName != "")
      {
         QString name = "libconnectionplugin.so";
         // QString source_dir = "../qdesigner";
         // QString plugin_dir = "../qdesigner/_output/plugins";
         QString source_dir = "./qdesigner";
         QString plugin_dir = "./_library/plugins";
         QString target_dir = plugin_dir + QDir::separator() + "designer";
         QString source = source_dir + QDir::separator() + name;
         QString target = target_dir + QDir::separator() + name;
         if ( ! QFile (target).exists ())
            if (QFile (source).exists ())
            {
               QDir (".").mkpath (target_dir);
               QFile::copy (source, target);
            }

         QProcessEnvironment env = QProcessEnvironment::systemEnvironment ();
         env.insert ("QT_PLUGIN_PATH", plugin_dir);

         QStringList args (fileName);

         // QProcess proc (this);
         // proc.start ("designer-qt5", args);
         QProcess * proc = new QProcess (this);
         proc->setProcessEnvironment (env);
         #if QT_VERSION < 0x060000
            proc->start ("designer-qt5", args);
         #else
            proc->start ("designer6", args);
         #endif
      }
   #endif
}

/* ---------------------------------------------------------------------- */

void Window::runInterpreter ()
{
    #ifdef USE_CLANG
      QString fileName = getCurrentFileName ();
      if (fileName != "")
      {
         string_list options;
         options.push_back (fileName.toStdString ());

         add_list (options, common_cflags ());

         string_list libraries;
         // add_list (libraries, common_libs ());

         compileAndRun (options,
                        libraries,
                        vector <string> (),
                        vector <string> ());

         cout << "O.K." << endl;
         cout.flush ();
         cerr.flush ();
      }
   #endif
}

/* ---------------------------------------------------------------------- */

void Window::runCompiler ()
{
   QString fileName = getCurrentFileName ();
   if (fileName != "")
   {
      QStringList args;
      args << fileName;

      // args << "-Xclang";
      // args << "-ast-dump";

      // args << "-S";
      // args << "-emit-llvm";
      // args << "-o";
      // args << "-";

      args << "-O0";
      args << "-g";

      args << "-I/usr/include/qt5";
      args << "-I/usr/include/qt5/QtCore";
      args << "-I/usr/include/qt5/QtGui";
      args << "-I/usr/include/qt5/QtWidgets";

      args << "-lQt5Core";
      args << "-lQt5Gui";
      args << "-lQt5Widgets";
      args << "-lstdc++";

      args << "-o";
      args << "output.bin";

      int len = args.size ();
      for (int inx = 0; inx < len; inx ++)
          cout << args [inx].toStdString () << " ";
      cout << endl;

      QProcess proc (this);
      proc.execute ("clang", args);

      proc.execute ("./output.bin", QStringList ());

      cout << "O.K." << endl;
   }
}

void Window::runDebugger ()
{
   #ifdef USE_LLDB
      // QString fileName = getCurrentFileName ();
      // QString fileName = "./a.out";
      QString fileName = "./qview.bin";
      if (fileName != "")
      {
         debug_with_lldb (this, fileName.toStdString ());
      }
   #endif
}

/* ---------------------------------------------------------------------- */

void Window::quit ()
{
   close ();
}

/* ---------------------------------------------------------------------- */

// global variables
static QHash <QString, int> FileNameDict;
static QHash <int, QString> FileNumberDict;
static int last_index = 0;

#ifndef USE_MINI
void clearFileNames ()
{
   FileNameDict.clear ();
   FileNumberDict.clear ();
}

int storeFileName (string file_name_str)
{
   QString file_name = QString::fromStdString (file_name_str);
   int index = 0;

   if (FileNameDict.contains (file_name))
   {
      index = FileNameDict.value (file_name);
   }
   else
   {
      last_index ++;
      index = last_index;
      FileNameDict.insert (file_name, index);
      FileNumberDict.insert (index, file_name);
   }

   return index;
}

string recallFileName (int index)
{
   return FileNumberDict.value (index, "").toStdString ();
   // return empty string if file index is not dictionay
}

int recallFileIndex (string file_name_str)
{
   QString file_name = QString::fromStdString (file_name_str);
   return FileNameDict.value (file_name, 0);
}
#endif

/* ---------------------------------------------------------------------- */

int main (int argc, char * argv [])
{
    #ifdef USE_TRACE
       set_error_handlers ();
    #endif

    #ifdef USE_LLDB_TRACE
       lldb::SBDebugger::PrintStackTraceOnError ();
    #endif

    QApplication a (argc, argv);
    Window w;

    w.show ();
    w.openEditor ("examples/example.ui");
    w.openEditor ("examples/example.cc");
    w.openEditor ("examples/example-qt.cc");
    w.openEditor ("examples/example-cls.cc");
    w.removeEmptyEditor ();

    return a.exec ();
}

/* ---------------------------------------------------------------------- */
