
/* trace.cc */

#include "trace.h"

// #define TRACE
// #define DEMANGLE
// #define LIBERTY_DEMANGLE
// #define BFD

// DEMANGLE ... C++ name demangling
// LIBERTY_DEMANGLE ... alternative demangle
// BFD ... line numberes, The Binary File Descriptor Library

// QMAKE_CXXFLAGS += -g # necessary for line numbers
// QMAKE_LFLAGS += -rdynamic # necessary for glibc backtrace function to show function names

// apt-get instal binutils-dev libltdl-dev
// dnf install binutils-devel libtool-ltdl-devel
// pacman -S libtool binutils

/* ---------------------------------------------------------------------- */

#ifdef TRACE
   #include <execinfo.h> // function backtrace
#endif

#ifdef DEMANGLE
   #include <cxxabi.h>
#endif

#ifdef LIBERTY_DEMANGLE
   #include <demangle.h>
#endif

#ifdef BFD
   #define PACKAGE_VERSION 1.0
   // ArchLinux

   // #define HAVE_DECL_BASENAME
   // #define __CORRECT_ISO_CPP_STRING_H_PROTO
   #define basename basename
   // Fedora

   #include <bfd.h>
   #include <dlfcn.h>

   #ifndef bfd_get_section_vma
      #define bfd_get_section_vma(bfd, ptr) ((void) bfd, (ptr)->vma)
   #endif

   #ifndef bfd_get_section_size
      #define bfd_get_section_size(ptr) ((ptr)->size)
   #endif

   #ifndef bfd_get_section_flags
      #define bfd_get_section_flags(bfd, ptr) ((void) bfd, (ptr)->flags)
   #endif
#endif

/* ---------------------------------------------------------------------- */

#include <malloc.h>
#include <signal.h>

#include <stdexcept>
#include <iostream>
// #include <fstream> // class ifstream
#include <sstream> // class ostringstream

using std::string;
using std::ifstream;
using std::ostringstream;
using std::cout;
using std::cerr;
using std::endl;
using std::set_terminate;

/* ---------------------------------------------------------------------- */

inline string IntToStr (int num)
{
   ostringstream stream;
   stream << num;
   return stream.str ();
}

inline string charsToStr (const char * c)
{
    return (c == NULL) ? "" : string (c);
}

inline string Head (const string s, int i)
{
   // first i characters
   return s.substr (0, i);
}

inline string Tail (const string s, int i)
{
   // skip first i characters
   int len = s.length ();
   if (i > len)
      return "";
   else
      return s.substr (i);
}

void show_abort (string s , string fileName="", int line=0, int col = 0)
{
   cerr << s << endl;
}

/* ---------------------------------------------------------------------- */

#ifdef BFD

/* from binutils/addr2line.c */

static asymbol * * syms = NULL;          /* Symbol table.  */

/* These global variables are used to pass information between
   translate_addresses and find_address_in_section.  */

static bfd_vma pc;
static const char * filename = NULL;
static const char * functionname = NULL;
static unsigned int line = 0;
static bool found = false;

void find_address_in_section (bfd *abfd, asection *section, void * data)
{
  bfd_vma vma;
  bfd_size_type size;

  if (found)
    return;

  if ((bfd_get_section_flags (abfd, section) & SEC_ALLOC) == 0)
    return;

  vma = bfd_get_section_vma (abfd, section);
  if (pc < vma)
    return;

  size = bfd_get_section_size (section);
  if (pc >= vma + size)
    return;

  found = bfd_find_nearest_line (abfd, section, syms, pc - vma,
                                 &filename, &functionname, &line);
}

static bool ready = true;
static bfd * abfd = NULL;

static string translate_address (long addr)
{
   string result = "";

   if (ready)
   {
      pc = addr;

      found = false;
      bfd_map_over_sections (abfd, find_address_in_section, NULL);

      if (found)
      {
         if (filename == NULL)
           result = "<unknonwn>";
         else
            result = filename;
         result = result + ":" + IntToStr (line);
         // result = result + ":" + functionname;
      }
   }

   return result;
}

static void initialize_bfd (string binary_file_name)
{
  abfd = NULL;
  syms = NULL;

  ready = true;
  // const char * target = "elf64-x86-64"; // "elf32-i386"; // !? list of targets: addr2line --help

  // bfd_set_default_target (target);

  // cerr << "<initialize_bfd> " << binary_file_name << endl;

  abfd = bfd_openr (binary_file_name.c_str (), NULL);
  if (abfd == NULL)
  {
     ready = false;
     return;
  }

  char **matching;
  if (! bfd_check_format_matches (abfd, bfd_object, &matching)) // important
  {
     ready = false;
     return;
  }

  if (bfd_check_format (abfd, bfd_archive))
  {
     ready = false;
     return;
  }

  long symcount = 0;
  unsigned int size = 0;

  if ((bfd_get_file_flags (abfd) & HAS_SYMS) == 0)
  {
     ready = false;
     return;
  }


  void * minisyms = NULL;

  symcount = bfd_read_minisymbols (abfd, FALSE, & minisyms, & size);
  if (symcount == 0)
     symcount = bfd_read_minisymbols (abfd, TRUE /* dynamic */, & minisyms, & size);

  if (symcount < 0)
     ready = false;

   syms = (asymbol **) minisyms;

   // cerr << "<bfd> " << symcount << " symbols" << endl;
}

void cleanup_bfd ()
{
  if (!ready)
     return;

  if (syms != NULL)
  {
      free (syms);
      syms = NULL;
  }

  if (abfd != NULL)
     bfd_close (abfd);
}

#endif

/* ---------------------------------------------------------------------- */

static string demangle (string txt)
{
   #ifdef DEMANGLE
      int status = 0;
      char * result =
         abi::__cxa_demangle
            (txt.c_str (), 0, 0, &status);

      if (result != NULL)
      {
         txt = result;
         free (result);
      }
   #else
      #ifdef LIBERTY_DEMANGLE
         cplus_demangle_set_style (auto_demangling);

         char * result =
            cplus_demangle
               (txt.c_str (),
                DMGL_PARAMS | DMGL_ANSI | DMGL_VERBOSE | DMGL_TYPES);

         if (result != NULL)
            txt = result;
      #endif
   #endif

   return txt;
}

/* ---------------------------------------------------------------------- */

void trace ()
{
   #ifdef TRACE
   const int table_max = 64;
   void * table [table_max];
   size_t table_len;

   // cerr << endl;

   table_len = backtrace (table, table_max);

   #ifndef BFD
      cerr << "<backtrace>" << endl;
      char * * text_table;
      text_table = backtrace_symbols (table, table_len);

      for (size_t i = 0; i < table_len; i++)
      {
         string txt = text_table [i];
         // cerr << txt << endl;

         string head = "";
         string tail = "";
         string pos = "";

         string::size_type inx = txt.find ('(');
         if (inx != string::npos)
         {
            head = Head (txt, inx);
            txt = Tail (txt, inx+1);
         }

         inx = txt.find (')');
         if (inx != string::npos)
         {
            tail = Tail (txt, inx+1);
            txt = Head (txt, inx);
         }

         inx = txt.find ('+');
         if (inx != string::npos)
         {
            pos = Tail (txt, inx+1);
            txt = Head (txt, inx);
         }

         txt = demangle (txt);

         cerr << txt;
         // cerr << " ... " << text_table [i];
         cerr << endl;
      }

      free (text_table);
      cerr << "<end of backtrace>" << endl;
   #endif // not BFD

   #ifdef BFD
      cerr << endl;
      cerr << "<bfd backtrace>" << endl;

      string func_table [table_max]; // function name
      string file_table [table_max]; // file name
      long   base_table [table_max]; // base address
      // long   addr_table [table_max]; // instruction address
      bool   valid_table [table_max]; // previous tables contains valid values

      string line_table [table_max]; // function name and line number as string
      bool   ready_table [table_max]; // line_table contains valid values

      bfd_init ();

      for (size_t i = 0; i < table_len; i++)
      {
         Dl_info info;
         int stat = dladdr (table[i], & info);
         if (stat)
         {
            func_table[i] = charsToStr (info.dli_sname);
            file_table[i] = charsToStr (info.dli_fname);
            base_table[i] = (long) info.dli_fbase;
            valid_table[i] = true;
         }
         else
         {
            func_table[i] = "";
            file_table[i] = "";
            base_table[i] = 0;
            valid_table[i] = false;
         }

         // clear tables
         line_table[i] = "";
         ready_table[i] = false;
      }

      for (size_t i = 0; i < table_len; i++)
      {
         if (! ready_table[i] && valid_table[i]) // not resolved and valid
         {
            initialize_bfd (file_table[i]); // new file name

            for (size_t j = i; j < table_len; j++)
            {
               if (! ready_table[j] && // not resolved
                   valid_table[j] &&  // valid
                   file_table[j] == file_table[i]) // same file name
               {
                   // use table [j] ... instruction address
                   // not addr_table[j] ... function address
                   long addr = (long) table[j];

                   string ans = translate_address (addr - base_table[j]);
                   if (ans == "")
                       ans = translate_address (addr);

                   line_table[j] = ans;
                   ready_table[j] = true; // resolved (or empty)
               }
            }
            cleanup_bfd ();
         }
      }

      for (size_t i = 0; i < table_len; i++)
      {
         if (line_table[i] != "")
            cerr << line_table[i] << ": ";
         cerr << demangle (func_table[i]);
         cerr << endl;
      }
      cerr << "<end of bfd backtrace>" << endl;
   #endif // BFD

   cerr << endl;
   #endif
}

/* ---------------------------------------------------------------------- */

#ifdef __linux__
extern "C"
void __assert_fail (const char * assertion,
                    const char * file,
                    unsigned int line,
                    const char * function)
{
   show_abort ("function: " + charsToStr (function) +
               ", assertion failed: " + charsToStr (assertion),
               file, line, 0);
}
#endif

/* ---------------------------------------------------------------------- */

#ifdef __linux__
extern "C"
void abort ()
{
   show_abort ("abort");
   raise (SIGTERM);
   exit (0);
}
#endif

/* ---------------------------------------------------------------------- */

#ifdef __linux__
void fpe_handler (int signum)
{
   show_abort ("Signal FPE, arithmetic exception");
   trace ();
   raise (SIGTERM);
}

void segv_handler (int signum)
{
   show_abort ("Signal SEGV, segmentation fault");
   trace ();
   raise (SIGTERM);
}

void abort_handler (int signum)
{
   show_abort ("Signal ABORT");
   raise (SIGTERM);
}

void terminate_subroutine ()
{
   show_abort ("unhandled exception => terminate");
   // no trace (), trace is called by abort_handler
}
#endif

/* ---------------------------------------------------------------------- */

void set_error_handlers ()
{
   #ifdef __linux__
      signal (SIGFPE,  fpe_handler);
      signal (SIGSEGV, segv_handler);
      signal (SIGABRT, abort_handler);
      set_terminate (terminate_subroutine);
   #endif
}

/* ---------------------------------------------------------------------- */
