/* grep.cc */

#include "grep.h"
#include "view.h"

#include <QLabel>
#include <QLineEdit>
#include <QGridLayout>
#include <QHeaderView>
#include <QDialogButtonBox>

#include <QFileInfo>

#include <iostream>
using namespace std;

/* ---------------------------------------------------------------------- */

GrepDialog::GrepDialog (GrepView * p_view) :
   QDialog (p_view),
   grep_view (p_view)
{
   setWindowTitle ("Find in Files");

   QGridLayout * layout = new QGridLayout (this);
   setLayout (layout);

   QLabel * label1 = new QLabel ("Find:", this);
   layout->addWidget (label1, 0, 0);

   pattern = new QLineEdit (this);
   layout->addWidget (pattern, 0, 1);

   QLabel * label2 = new QLabel ("Case Sensitive:", this);
   layout->addWidget (label2, 1, 0);

   caseSensitive = new QCheckBox (this);
   layout->addWidget (caseSensitive, 1, 1);

   QLabel * label3 = new QLabel ("Whole words:", this);
   layout->addWidget (label3, 2, 0);

   wholeWords = new QCheckBox (this);
   layout->addWidget (wholeWords, 2, 1);

   QLabel * label4 = new QLabel ("Regular expression:", this);
   layout->addWidget (label4, 3, 0);

   regularExpression = new QCheckBox (this);
   layout->addWidget (regularExpression, 3, 1);

   QLabel * label5 = new QLabel ("Directory:", this);
   layout->addWidget (label5, 4, 0);

   directory = new QComboBox (this);
   directory->setEditable (true);
   directory->addItem (".");
   directory->addItem ("..");
   layout->addWidget (directory, 4, 1);

   QDialogButtonBox * box = new QDialogButtonBox (QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
   layout->addWidget (box, 5, 1);

   connect (box, &QDialogButtonBox::accepted, this, &GrepDialog::search);
   connect (box, &QDialogButtonBox::rejected, this, &GrepDialog::reject);
   show ();
}


void GrepDialog::search ()
{
   QString params = pattern->text ();
   params = params.trimmed ();
   if (grep_view != NULL && params != "")
   {
      if (! caseSensitive->isChecked ())
         params = "-i " + params;

      if (wholeWords->isChecked ())
         params = "-w " + params;

      if (! regularExpression->isChecked ())
         params = "-F " + params;

      QString path = directory->currentText ();
      path = path.trimmed ();
      if (path == "")
          path = ".";
      params = params + " -R " + path;

      accept ();

      grep_view->grep (params);
   }
}

/* ---------------------------------------------------------------------- */

GrepView::GrepView (Window * p_win) :
    QWidget (p_win),
    win (p_win),
    process (NULL),
    branch (NULL)
{
    initVariables ();

    QVBoxLayout * layout = new QVBoxLayout (this);
    setLayout (layout);

    tree = new Tree (win);
    tree->header ()->hide ();
    layout->addWidget (tree);

    stopButton = new QPushButton (this);
    stopButton->setText ("stop");
    stopButton->setEnabled (false);
    connect (stopButton, &QPushButton::clicked, this, &GrepView::onStopButton);
    layout->addWidget (stopButton);
}

void GrepView::initVariables ()
{
    lastFileName = "";
    branch = NULL;
    initLineVariables ();
}

void GrepView::initLineVariables ()
{
    startLine = true;
    middle = false;
    fileName = "";
    lineNum = 0;
    text = "";
}

void GrepView::onStartProcess ()
{
   stopButton->setEnabled (true);
}

void GrepView::onStopProcess ()
{
   stopButton->setEnabled (false);
}

void GrepView::onStopButton ()
{
   if (process != NULL)
      process->terminate ();
}

void GrepView::showItem ()
{
    if (text != "")
    {
        if (fileName != lastFileName)
            branch = NULL;

        if (branch == NULL)
        {
            branch = new TreeItem (tree);
            branch->setText (0, fileName);
            branch->setForeground (0, QColor (Qt::darkGreen));
            QFileInfo fi (fileName);
            branch->src_file = storeFileName (fi.absoluteFilePath().toStdString());
            lastFileName = fileName;
        }

        TreeItem * node = new TreeItem (branch);
        node->setText (0, "Line " + QString::number (lineNum) + ": " + text);
        node->src_file = branch->src_file;
        node->src_line = lineNum;
    }
}

void GrepView::onDataReady ()
{
   QByteArray data = process->readAll ();
   int len = data.size ();
   for (int inx = 0; inx < len; inx ++)
   {
       char c = data [inx];
       if (c == '\n')
       {
          showItem ();
          initLineVariables ();
       }
       else if (startLine)
       {
          if (c != ':')
          {
             fileName += c;
          }
          else
          {
             startLine = false;
             middle = true;
          }
       }
       else if (middle)
       {
          if (c != ':')
          {
             if (c >= '0' && c <= '9')
                lineNum = 10 * lineNum + (c - '0');
          }
          else
          {
              middle = false;
          }
       }
       else
       {
           text += c;
       }
   }
}

void GrepView::prevItem ()
{
   QTreeWidgetItem * item = tree->currentItem ();
   if (item == NULL)
   {
      item = tree->topLevelItem (0);
      if (item != NULL)
      {
         tree->setCurrentItem (item);
         // onItemActivated (item, 0);
      }
   }
   else
   {
      #if QT_VERSION >= 0x040300
      QTreeWidgetItem * temp = tree->itemAbove (item);

      if (temp != NULL)
         tree->expandItem (temp);

      item = tree->itemAbove (item);
      if  (item != NULL)
      {
         tree->setCurrentItem (item);
         // onItemActivated (item, 0);
      }
      #endif
   }
}

void GrepView::nextItem ()
{
   QTreeWidgetItem * item = tree->currentItem ();
   if (item == NULL)
   {
      item = tree->topLevelItem (0);
      if (item != NULL)
      {
         tree->setCurrentItem (item);
         // onItemActivated (item, 0);
      }
   }
   else
   {
      #if QT_VERSION >= 0x040300
      QTreeWidgetItem * temp = tree->itemBelow (item);

      if (temp != NULL)
          tree->expandItem (temp);

      item = tree->itemBelow (item);

      if (item != NULL)
      {
         tree->setCurrentItem (item);
         // onItemActivated (item, 0);
      }
      #endif
   }
}

void GrepView::grep (QString params)
{
   tree->clear ();
   initVariables ();

   if (process != NULL) delete process;

   process = new QProcess (this);
   process->setProcessChannelMode (QProcess::MergedChannels);
   connect (process, &QProcess::readyRead, this, &GrepView::onDataReady);
   connect (process, &QProcess::started, this, &GrepView::onStartProcess);
   #if QT_VERSION < 0x050b00
      connect (process,static_cast <void(QProcess::*)(int, QProcess::ExitStatus)> (&QProcess::finished), this, &GrepView::onStopProcess);
   #else
      connect (process, qOverload <int, QProcess::ExitStatus> (&QProcess::finished), this, &GrepView::onStopProcess);
   #endif
   // http://stackoverflow.com/questions/12534367/no-slot-qprocessfinished-signal
   params = "-n " + params;

   QStringList args;
   args << "-c" << "grep " + params + " -r .";
   process->start ("/bin/sh", args);
}

/* ---------------------------------------------------------------------- */

// SIGNAL/SLOT without parameters pattern: \b%s\b\s*\([^\(]*\)

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
