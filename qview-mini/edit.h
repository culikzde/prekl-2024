
/* edit.h */

#ifndef EDIT_H
#define EDIT_H

#include <QTextEdit>
#include <QCompleter>
#include <QAction>

#include <QHBoxLayout>
#include <QLabel>
#include <QComboBox>
#include <QSpinBox>
#include <QCheckBox>
#include <QToolButton>
#include <QPushButton>

#include <QDateTime>

/* ---------------------------------------------------------------------- */

class Bookmark
{
   public:
      int line;
      int column;
      int mark;

      Bookmark () : line (0), column (0), mark (0) { }
};

typedef QList <Bookmark> BookmarkList;

class FindBox;
class GoToLineBox;

enum EditFunc { indentFunc, unindentFunc, commentFunc, uncommentFunc };

/* ---------------------------------------------------------------------- */

class Edit : public QTextEdit
{
   Q_OBJECT
   public:
      explicit Edit (QWidget * parent);
      // virtual ~ Edit ()

   // Code completion

   public:
      void select (int line, int column = 0, int endLine = 0, int endColumn = 0);

   protected:
      void keyPressEvent (QKeyEvent *e) override;
      void focusInEvent (QFocusEvent *e) override;

   private slots:
      void insertCompletion (const QString &completion);

   private:
      QCompleter * completer;

   private:
      void setCompleter (QCompleter * c);
      QString textUnderCursor () const;

   private:

      void editorFunction (EditFunc func);

      QString getCommentMark ();

   // Comment, Indent, Boomkarks

   public:
      void comment ();
      void uncomment ();

      void indent ();
      void unindent ();

      bool bookmarkKeyPressEvent (QKeyEvent * e);

      void setBookmark (int markType = 1);
      void gotoBookmark (int markType = 1);
      void gotoPrevBookmark (int markType=1);
      void gotoNextBookmark (int markType=1);
      void clearBookmarks ();

      BookmarkList getBookmarks ();
      void setBookmarks (BookmarkList bookmarks);

   // Edit

   private:
      QTextCursor lastCursor;
      bool lastUnderline;
      QColor lastUnderlineColor;
      bool closeWithoutQuestion;

   public:
      FindBox * findBox;
      GoToLineBox * lineBox;

   private:
      QString origFileName;
      QDateTime origTimeStamp;
      QString origText;

      void init ();
      void showStatus (QString text);
      void onCursorPositionChanged ();
      void selectLineByPosition (int pos);
      // void select2 (int start_line, int start_col, int stop_line, int stop_col);
      bool event (QEvent * e) override;
      void mouseMoveEvent (QMouseEvent * e) override;
      void mousePressEvent (QMouseEvent * e) override;

      void jumpToObject (QObject * obj);
      void * findGlobalData ();
      void * findIdentifier (QString qual_name);
      QString findContext (QTextCursor cursor);
      void displayProperty (QMenu * menu, QTextCharFormat format, QString title, int property);

   protected:
      void contextMenuEvent (QContextMenuEvent * e) override;
      QStringList getCompletionList ();
      QString getCursorInfo ();

   public:
      void showMemo (QString name="");
      void showReferences (QString name="");

      void gotoNextFunction ();
      void gotoPrevFunction ();

      void gotoBegin ();
      void gotoEnd ();
      void gotoPageUp ();
      void gotoPageDown ();

      void scrollUp ();
      void scrollDown ();
      void scrollLeft ();
      void scrollRight ();

      void enlargeFont ();
      void shrinkFont ();

      void moveLines (bool up);
      void moveLinesUp ();
      void moveLinesDown ();

   public:
      void selectLine (int line);

   private:
      void init2 ();

   // Find, Replace

   public slots:
      void findText ();
      void findNext ();
      void findPrev ();
      void replaceText ();
      void findSelected ();
      void findIncremental ();
      void goToLine ();

   // Open, Save

   public:
      QString getFileName ();
      void setFileName (QString fileName) { origFileName = fileName; }
      // void simpleReadFile (QString fileName);
      QString readFileData (QString fileName);
      void readFile (QString fileName);
      void writeFile (QString fileName);
      bool isModified ();
      bool isModifiedOnDisk ();
      void openFile ();
      void saveFile ();
      void saveFileAs ();
      void reloadFile ();
};

/* ---------------------------------------------------------------------- */

class FindBox: public QWidget
{
   Q_OBJECT
   private:
      QHBoxLayout * horiz_layout;
      QToolButton * closeButton;
      QLabel * label;
      QToolButton * clearButton;
      QCheckBox * wholeWords;
      QCheckBox * matchCase;
      QPushButton * nextButton;
      QPushButton * prevButton;

   private:
      bool small;
      QColor red;
      QColor green;
      int start;

   public:
      QComboBox * line;
      Edit * edit;
      bool incremental;

   public slots:
      // void hide ();
      void returnPressed ();
      void textChanged (QString text);
      void findNext ();
      void findPrev ();

   public:
      FindBox (QWidget * parent = NULL);
      void open ();

      void resizeEvent (QResizeEvent * e) override;
      void findStep (bool back);
};

/* ---------------------------------------------------------------------- */

class GoToLineBox: public QWidget
{
   Q_OBJECT
   private:
      QHBoxLayout * horiz_layout;
      QToolButton * closeButton;
      QLabel * label;
      QSpinBox * line;
      QPushButton * button;

   public:
      Edit * edit;

   public slots:
      void editingFinished ();
      // void hide ();

   public:
      GoToLineBox (QWidget * parent = NULL);
      void open ();
};

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all

#endif // EDIT_H
