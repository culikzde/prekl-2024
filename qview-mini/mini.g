
/* tkit/cmm.g */

options
{
   /* output file names */

   common_header = cmm_data; // common header (.hpp) (enumerations, class names)
   common_name = cmm_data; // common code (.cpp)

   public_header  = cmm_public;  // public header (.hpp) (classes)
   private_header = cmm_private; // private header (.hpp) (private classes)

   // symbol_header = cmm_symbol; // symbol type header (.hpp) (enumeration)

   lexer_name = cmm_lexer; // output file name prefix (.gpp, .hpp, .cpp)
   parser_name = cmm_parser; // output file name prefix (.gpp, .hpp, .cpp)

   product_name = cmm_product; // output file name prefix (.hpp, .cpp)

   /* output file names */

   html_output = "tkit-html.html";
   lout_output = "tkit-lout.lout";

   /* classes and namespaces */

   // product_namespace = finch;
   automatic_namespace = true;

   lexer_class = CmmLexer;
   lexer_super_class = TEnvInput;

   parser_class = CmmParser;
   // parser_super_class = TEnvInput;

   product_class = CmmProduct;
   product_super_class = CmmOutput;

   /* options */

   // tree_code = true; // enable / disable 'produce_...' methods

   expr_method = true; // optimized binary expressions

   // notes = true; // store comments and spaces
   // only_start_notes = true; // do not store comments after tokens
   // fixed_notes = true; // true .. variables, not array

   locations = true; // store line numbers

   // ignore_notes = true; // true => do not store notes, only declare data structures
   // ignore_locations = true; // true => do not store locations, only declare data structures

   // note_type = CmmNote; // type identifier for notes
   location_type = CmmLocation; // type identifier for locations
   mark_type = CmmMark; // type identifier for mark / rewind

   // note_init = "\"\""; // initialization value for notes
   location_init = "NULL"; // initialization value for locations

   // anonymous_fields = true;
   // ignore_send_options = true; // no custom send, ...
   // ignore_output_options = true; // no custom output, internal_output
   // ignore_execute_options = true; // no execute methods
   // ignore_product_constructor = true; // only for gram
   // ignore_tag_marks = true; // ignore '<' separators, only for gram

   // subst_type = CmmDefineItem; // class identifier for substitution items

   // without_try = true; // true => parser without try / catch
   // with_free = false; // true => delete data

   /* identifiers */

   /*
   rule_prefix = rule_ ; // rule names (in parser class)
   type_prefix = type_ ; // type names // !?

   field_prefix = field_ ; // user defined fields (in structures)
   list_prefix = queue_ ; // fields and routines for sequences (in structures)
   internal_prefix = internal_ ; // internal fields and routines (in structures)

   send_func_prefix = send_fce_ ; // send text output functions
   send_internal_prefix = original_ ; // original send text output functions
   open_func_prefix = open_fce_ ; // open functions (<custom open_close_send> )
   close_func_prefix = close_fce_ ; // open functions (<custom open_close_send> )

   conv_func_prefix = convert_ ; // enumeration type conversion functions
   util_func_prefix = utility_ ; // support functions

   param_prefix = parameter_ ; // parameters
   // temp_prefix = temporary_ ; // not used - temporary variables
   // local_prefix = local_ ; // not used - local variables
   variable_prefix = variable_ ; // variables: item, item1, subitem, result
   */

   /* symbol type */

   symbol_type = CmmSymbol;
   conv_type = int; // type used as parameter for "conv" method
   no_symbol = NONE;

   // primitive_prefix = simple_ ;
   // delimiter_prefix = delimiter_ ;
   // anonymous_delimiter_prefix = anonymous_literal_ ;
   // keyword_prefix = keyword_ ;

   /* binary and unary expressions */

   basic_type = CmmBasic;
   expr_type = CmmExpr;

   binary_expr_type     = CmmBinaryExpr;
   binary_expr_left     = left;
   binary_expr_right    = right;
   binary_expr_middle   = middle;
   binary_expr_selector = kind;
   binary_expr_func     = bin_expr;

   unary_expr_type     = CmmUnaryExpr;
   unary_expr_param    = param;
   unary_expr_selector = kind;
}

/* --------------------------------------------------------------------- */

/* extension of cmm_data.hpp */

extension common_include
{
}

extension common_header
{
class TInputItem;
typedef TInputItem * CmmMark;
typedef TInputItem * CmmLocation;
}

/* --------------------------------------------------------------------- */

/* extension of cmm_lexer.h and CmmLexer class */

extension lexer_include
{
#include "input.h"
#include "ioexception.h"
}

extension lexer_header
{
}

class CmmLexer
{
public:
   virtual void translate ();
}

/* --------------------------------------------------------------------- */

/* extension of cmm_parser.h and CmmParser class */

extension parser_include
{
}

extension parser_header
{
}

class CmmParser
{
}

/* --------------------------------------------------------------------- */

/* extension of cmm_product.h and CmmProduct class */

extension product_include
{
   #include "output.h"
}

extension product_header
{
}

/* --------------------------------------------------------------------- */

literals // identifiers for C++ literals
{
   LPAREN     : '(' ;
   RPAREN     : ')' ;
   LBRACKET   : '[' ;
   RBRACKET   : ']' ;
   LBRACE     : '{' ;
   RBRACE     : '}' ;

   DOT        : '.'  ;
   ARROW      : "->" ;
   SCOPE      : "::" ;

   LOG_NOT    : '!' ;
   BIT_NOT    : '~' ;

   INC        : "++"  ;
   DEC        : "--"  ;

   DOT_STAR   : ".*"  ;
   ARROW_STAR : "->*" ;

   STAR       : '*' ;
   SLASH      : '/' ;
   MOD        : '%' ;

   PLUS       : '+' ;
   MINUS      : '-' ;

   SHL        : "<<" ;
   SHR        : ">>" ;

   LESS             : '<'  ;
   GREATER          : '>'  ;
   LESS_OR_EQUAL    : "<=" ;
   GREATER_OR_EQUAL : ">=" ;

   EQUAL            : "==" ;
   UNEQUAL          : "!=" ;

   BIT_AND    : '&' ;
   BIT_XOR    : '^' ;
   BIT_OR     : '|' ;

   LOG_AND    : "&&" ;
   LOG_OR     : "||" ;

   ASSIGN         : '='   ;
   MUL_ASSIGN     : "*="  ;
   DIV_ASSIGN     : "/="  ;
   MOD_ASSIGN     : "%="  ;
   PLUS_ASSIGN    : "+="  ;
   MINUS_ASSIGN   : "-="  ;
   SHL_ASSIGN     : "<<=" ;
   SHR_ASSIGN     : ">>=" ;
   BIT_AND_ASSIGN : "&="  ;
   BIT_XOR_ASSIGN : "^="  ;
   BIT_OR_ASSIGN  : "|="  ;

   QUESTION   : '?' ;
   COMMA      : ',' ;

   COLON      : ':' ;
   SEMICOLON  : ';' ;
   DOTS       : "..." ;
}

/* --------------------------------------------------------------------- */

// CmmAccess type shared by CmmMemberSect and CmmBaseItem

enum CmmAccess
{
   NoAccess, // default value
   PrivateAccess,
   ProtectedAccess,
   PublicAccess
};

struct CmmMemberVisibility
{
   CmmAccess access;
};

struct CmmBaseItem
{
   CmmAccess access;
};

/* --------------------------------------------------------------------- */

parser ;

/* --------------------------------------------------------------------- */

identifier <primitive> <identifier> <custom send>:
  IDENT ;

numeric_literal <primitive>:
  NUMBER ;

char_literal <primitive>:
  CHAR_LITERAL ;

string_literal <primitive>:
  STRING_LITERAL ;

/* --------------------------------------------------------------------- */

/* compound name */

base_subst <modify CmmName> <custom subst>:
  id: identifier;

base_name <modify CmmName>:
  (
     spec_func: special_function
  |
     <call> base_subst // !? last - important
  );

base_args <modify CmmName>:
  (
    (template_arg_list) =>
       template_args:template_arg_list
  )? ;

cont_item <CmmContName>:
  <no_space> "::" <no_space>
  <call> cont_name
  <call> cont_args ;

cont_subst <modify CmmContName> <custom subst>:
  id: identifier;

cont_name <modify CmmContName>:
  (
     '~' <no_space> <set a_destructor = true>
  |
     "template" <set a_template = true>
  )?
  (
     spec_func: special_function
  |
     <call> cont_subst // !? last - important
  );

cont_args <modify CmmContName>:
  (
     (template_arg_list) =>
        template_args:template_arg_list
  )? ;

/* simple name */

simple_name <CmmName>:
  <call> base_subst // only identifier
  ;

/* qualified name */

qualified_name <CmmName> <custom list_subst>:
  ( "::" <set a_global = true> )?
  <call> base_name
  <call> base_args
  ( <add> cont_item )*
  ;

/* --------------------------------------------------------------------- */

/* id expr */

id_expr <return CmmName:CmmExpr>:
   qualified_name;

/* primary expression */

primary_expr <select CmmExpr>:
    numeric_literal_expr
  | char_literal_expr
  | string_literal_expr
  | this_expr
  | subexpr_expr
  | id_expr // !? last - important
  ;

numeric_literal_expr <CmmNumValue:CmmExpr>:
  value:numeric_literal;

char_literal_expr <CmmCharValue:CmmExpr>:
  value:char_literal;

string_literal_cont <CmmStringCont>:
  value:string_literal;

string_literal_expr <CmmStringValue:CmmExpr>:
  value:string_literal
  ( <add> string_literal_cont )*; // !?

this_expr <CmmThisExpr:CmmExpr>:
  "this";

subexpr_expr <CmmSubexprExpr:CmmExpr>:
  '(' param:expr ')';

/* postfix expression */

postfix_start <select CmmExpr>:
    modern_cast_expr
  | typeid_expr
  | typename_expr // !?
  | type_change_expr // !?
  | primary_expr ; // !? last - important

modern_cast_expr <CmmModernCastExpr:CmmExpr>:
  ( "dynamic_cast"       <set kind = DynamicCast>
  | "static_cast"        <set kind = StaticCast>
  | "const_cast"         <set kind = ConstCast>
  | "reinterpret_cast"   <set kind = ReinterpreterCast> )
  '<' <no_space> type:type_id <no_space> '>'
  '(' param:expr ')'
  ;

typeid_expr <CmmTypeidExpr:CmmExpr>:
  "typeid"
  '('
  (
     (type_id) =>
        type:type_id
  |
     value:expr
  )
  ')'
  ;

typename_expr <CmmTypenameExpr:CmmExpr>:
  "typename"
  name:qualified_name
  '(' list:expr_list ')' ;

type_change_expr <CmmTypechangeExpr:CmmExpr>: // !?
  (
    "signed"   <set a_signed = true>   |
    "unsigned" <set a_unsigned = true> |

    "short"    <inc a_short>           |
    "long"     <inc a_long>            |

    "bool"     <set a_bool = true>     |
    "char"     <set a_char = true>     |
    "wchar_t"  <set a_wchar = true>    |
    "int"      <set a_int = true>      |
    "float"    <set a_float = true>    |
    "double"   <set a_double = true>   |
    "void"     <set a_void = true>
  )
  '(' list:expr_list ')' ;

postfix_expr <choose CmmExpr>:
  postfix_start
  (
    index_expr |
    call_expr |
    field_expr |
    ptr_field_expr |
    post_inc_expr |
    post_dec_expr
  )* ;

index_expr <CmmIndexExpr:CmmExpr>:
  <store left:CmmExpr>
  '[' right:expr ']' ;

call_expr <CmmCallExpr:CmmExpr>:
  <store param:CmmExpr>
  '(' list:expr_list ')' ;

field_expr <CmmFieldExpr:CmmExpr>:
  <store param:CmmExpr>
  <no_space> '.' <no_space>
  (
     '~' <no_space> <set destructor_name = true>
  |
     "template" <set template_name = true>
  )?
  name:qualified_name ;

ptr_field_expr <CmmPtrFieldExpr:CmmExpr>:
  <store param:CmmExpr>
  <no_space> "->" <no_space>
  (
     '~' <no_space> <set destructor_name = true>
  |
     "template" <set template_name = true>
  )?
  name:qualified_name ;

post_inc_expr <CmmPostIncExpr:CmmExpr>:
  <store param:CmmExpr>
  "++" ;

post_dec_expr <CmmPostDecExpr:CmmExpr>:
  <store param:CmmExpr>
  "--" ;

expr_list <CmmExprList> <custom subst>:
  ( <add> assignment_expr (',' <add> assignment_expr)* )? ;

/* unary expression */

unary_expr <select CmmExpr>:
    inc_expr
  | dec_expr
  | deref_expr
  | addr_expr
  | plus_expr
  | minus_expr
  | bit_not_expr
  | log_not_expr
  | scope_expr // !?
  | sizeof_expr
  | allocation_expr
  | deallocation_expr
  | throw_expr // !?
  | postfix_expr ; // !? last - important

inc_expr <unary>:
  "++" IncExp cast_expr;

dec_expr <unary>:
  "--" DecExp cast_expr;

deref_expr <unary>:
  '*' DerexExp cast_expr;

addr_expr <unary>:
  '&' AddrExp cast_expr;

plus_expr <unary>:
  '+' PlusExp cast_expr;

minus_expr <unary>:
  '-' MinusExp cast_expr;

bit_not_expr <unary>:
  '~' BitNotExp cast_expr;

log_not_expr <unary>:
  '!' LogNotExp cast_expr;

scope_expr <unary>: // !?
   "::" ScopeExp cast_expr ;

sizeof_expr <CmmSizeofExp:CmmExpr>:
  "sizeof"
  (
     ( '(' type_id ')' ) =>
       '(' type:type_id ')'
  |
     value:unary_expr
  );

/* new */

allocation_expr <CmmNewExpr:CmmExpr>:
  // ( "::" <set a_global = true> )?
  "new"
  // (
  //    placement:new_placement
  // )?
  (
     type1:new_type_id
  |
    '(' type2:type_id ')'
  )
  (
    '(' init:expr_list ')'
  )? ;

// new_placement <CmmNewPlacement>:
//   '(' list:expr_list ')' ;

new_type_id <CmmNewTypeId>:
  type_spec:type_specifiers
  ptr:ptr_specifier_list
  ( <add> allocation_array_limit )* ;

allocation_array_limit <CmmNewArrayLimit>:
  '[' value:expr ']'  ;

/* delete */

deallocation_expr <CmmDeleteExpr:CmmExpr>:
  // ( "::" <set a_global = true> )?
  "delete"
  ( '[' ']' <set a_array = true> )?
  param:cast_expr ;

/* cast expression */

cast_expr <select CmmExpr>:
  //   ( cast_formula ) => // !?
  //      cast_formula
  // |
    unary_expr ;

// cast_formula <CmmCastFormula: CmmExpr>:
//   '(' type:type_id ')' param:cast_expr ;

/* pm expression */

pm_expr <binary>:
  cast_expr
  ".*"  DotMemberExp ,
  "->*" ArrowMemberExp ;

/* arithmetic and logical expressions */

multiplicative_expr <binary>:
  pm_expr
  '*' MulExp ,
  '/' DivExp ,
  '%' ModExp ;

additive_expr <binary>:
  multiplicative_expr
  '+' AddExp ,
  '-' SubExp ;

shift_expr <binary>:
  additive_expr
  "<<" ShlExp ,
  ">>" ShrExp ;

relational_expr <binary>:
  shift_expr
  '<' LtExp ,
  '>' GtExp ,
  "<=" LeExp ,
  ">=" GeExp ;

equality_expr <binary>:
  relational_expr
  "==" EqExp ,
  "!=" NeExp ;

and_expr <binary>:
  equality_expr
  '&' BitAndExp ;

exclusive_or_expr <binary>:
  and_expr
  '^' BitXorExp ;

inclusive_or_expr <binary>:
  exclusive_or_expr
  '|' BitOrExp ;

logical_and_expr <binary>:
  inclusive_or_expr
  "&&" LogAndExp ;

logical_or_expr <binary>:
  logical_and_expr
  "||" LogOrExp ;

/* conditional and assignment expression */

assignment_expr <binary>:
  logical_or_expr // !?
  '='   AssignExp    ,
  "+="  AddAssignExp ,
  "-="  SubAssignExp ,
  "*="  MulAssignExp ,
  "/="  DivAssignExp ,
  "%="  ModAssignExp ,
  "<<=" ShlAssignExp ,
  ">>=" ShrAssignExp ,
  "&="  AndAssignExp ,
  "^="  XorAssignExp ,
  "|="  OrAssignExp  ,
  '?'   CondExp ( expr ':' ) ; // logical_or_expr ? expr : logical_or_expr

conditional_expr <return CmmExpr>:
  assignment_expr ;

/* expression */

expr <binary>:
  assignment_expr
  ',' CommaExp ;

/* constant expression */

const_expr <return CmmExpr>:
  conditional_expr ;

/* --------------------------------------------------------------------- */

/* statement */

stat <select CmmStat> <custom list_subst>:

     ( identifier ':' ) => labeled_stat |
     ( block_declaration_head ) => declaration_stat |
     expression_stat |
     empty_stat |

     compound_stat |
     case_stat |
     default_stat |
     if_stat |
     switch_stat |
     while_stat |
     do_stat |
     for_stat |
     break_stat |
     continue_stat |
     return_stat |
     goto_stat |
     try_stat;

/* inner_stat and tail_stat - statements with different output indentation */

inner_stat <return CmmStat> <custom send>:
   <indent> stat <unindent>;

tail_stat <return CmmStat> <custom send>:
   <indent> stat <unindent>;

/* list of statements */

stat_list <CmmStatSect>:
   (  <new_line> <add> stat )* ;

inner_stat_list <return CmmStatSect> <custom send>:
   <indent>
   stat_list
   <unindent> ;

/* conditions */

condition <CmmCondition>:
     ( condition_declaration ')' ) =>
        cond_decl:condition_declaration
  |
     cond_expr:expr
  ;

for_condition <CmmCondition>:
     ( condition_declaration ';' ) =>
        cond_decl:condition_declaration
  |
     cond_expr:expr
  ;

/* concrete statements */

declaration_stat <CmmDeclStat:CmmStat> <custom subst>:
  inner_decl:block_declaration ;

labeled_stat <CmmLabeledStat:CmmStat>:
  lab:identifier ':'
  <new_line>
  body:stat ;

case_stat <CmmCaseStat:CmmStat>:
  "case" case_expr:expr ':'
  <new_line>
  body:stat;

default_stat <CmmDefaultStat:CmmStat>:
  "default" ':'
  <new_line>
  body:stat;

expression_stat <CmmExprStat:CmmStat> <custom subst>:
  inner_expr:expr ';';

empty_stat <CmmEmptyStat:CmmStat>:
  ';' ;

compound_stat <CmmCompoundStat:CmmStat> <custom open_close_send>:
  '{'
  body:inner_stat_list
  '}' ;

if_stat <CmmIfStat:CmmStat>:
  "if" '(' cond:condition ')' then_stat:inner_stat
  (
    <silent>
    <new_line>
    "else" else_stat:tail_stat
  )? ;

switch_stat <CmmSwitchStat:CmmStat>:
  "switch" '(' cond:condition ')' body:inner_stat ;

while_stat <CmmWhileStat:CmmStat>:
  "while" '(' cond:condition ')' body:inner_stat ;

do_stat <CmmDoStat:CmmStat>:
  "do" body:inner_stat "while" '(' cond_expr:expr ')' ';' ;

for_stat <CmmForStat:CmmStat>:
  "for"
  '('
  ( from_expr:for_condition )?
  ';'
  (to_expr:expr)?
  ';'
  (cond_expr:expr)?
  ')'
  body:inner_stat ;

break_stat <CmmBreakStat:CmmStat>:
  "break" ';';

continue_stat <CmmContinueStat:CmmStat>:
  "continue" ';';

return_stat <CmmReturnStat:CmmStat>:
  "return" (return_expr:expr)? ';';

goto_stat <CmmGotoStat:CmmStat>:
  "goto" goto_lab:identifier ';';

/* ---------------------------------------------------------------------- */

/* declaration */

declaration_list <CmmDeclSect>:
  ( <add> declaration <new_line> )* ;

declaration <select CmmDecl> <custom list_subst>:

    ( "extern" string_literal ) => linkage_declaration
  | ( constructor_head ) => constructor_declaration
  | simple_declaration
  | empty_declaration

  | class_declaration
  | enum_declaration

  | typedef_declaration
  | friend_declaration

  | namespace_declaration
  | using_declaration
  | asm_declaration

  | template_declaration

  ;

/* block declaration - used only in declaration_stat */

block_declaration <select CmmDecl>:

    simple_declaration
  | typedef_declaration
  | using_declaration
  ;

/* block declaration head - only for predicate */

block_declaration_head <CmmLocalHead> :
    dcl1:local_head
  | "typedef"   <set dcl2=true>
  | "using"     <set dcl3=true>
  ;

/* ---------------------------------------------------------------------- */

/* namespace declaration */

namespace_declaration <CmmNamespaceDecl:CmmDecl> :
  "namespace"
  (
     name:simple_name
     (
        '=' value:qualified_name ';'
     |
        <call> namespace_body
     )
  |
     <call> namespace_body
  );

namespace_body <modify CmmNamespaceDecl> :
   <new_line>
   '{'
      <indent>
      body:declaration_list
      <unindent>
   '}'
   ;

/* using declaration */

using_declaration <CmmUsingDecl:CmmDecl>:
  "using"
  (
     "namespace" <set a_namespace = true>
  |
     "typename"  <set a_typename  = true>
  )?
  name: qualified_name
  ';' ;

/* linkage declaration */

linkage_declaration <CmmExternDecl:CmmDecl>:
  "extern"
  language:string_literal
  (
     '{' decl_list:declaration_list '}'
  |
     inner_decl:declaration
  ) ;

/* typedef declaration */

typedef_declaration <CmmTypedefDecl:CmmDecl>:
  "typedef"
  type_spec:type_specifiers
  decl:declarator
  ';' ;

/* friend declaration */

friend_declaration <CmmFriendDecl:CmmDecl>:
  "friend"
  inner_decl:declaration ;

/* asm declaration */

asm_declaration <CmmAsmDecl:CmmDecl>:
  "asm" '(' instruction:string_literal ')' ';' ;

/* empty declaration */

empty_declaration <CmmEmptyDecl:CmmDecl> <custom send>:
  ';' ;

/* ---------------------------------------------------------------------- */

/* declaration specifiers */

decl_specifiers <CmmDeclSpec>:
  (
    "inline"   <set a_inline = true>   |
    "virtual"  <set a_virtual = true>  |
    "explicit" <set a_explicit = true> |
    "mutable"  <set a_mutable = true>  |

    "extern"   <set a_extern = true>   |
    "static"   <set a_static = true>   |
    "auto"     <set a_auto = true>     |
    "register" <set a_register = true>
  )* ;

/* type specifiers */

type_specifiers <CmmTypeSpec>:
  (
    "const" <set a_const = true>
  |
    "volatile" <set a_volatile = true>
  )?
  (
     basic_name:qualified_name
  |
     "typename" typename_name:qualified_name
  |
     <call> simple_type_specifiers
  );

simple_type_specifiers <modify CmmTypeSpec>:
  ( "signed"   <set a_signed = true>   |
    "unsigned" <set a_unsigned = true> |

    "short"    <inc a_short>           |
    "long"     <inc a_long>            |

    "bool"     <set a_bool = true>     |
    "char"     <set a_char = true>     |
    "wchar_t"  <set a_wchar = true>    |
    "int"      <set a_int = true>      |
    "float"    <set a_float = true>    |
    "double"   <set a_double = true>   |
    "void"     <set a_void = true>
  )@ ;

/* ---------------------------------------------------------------------- */

/* pointer specifiers */

ptr_specifier_list <CmmPtrSpecifierSect>:
  ( <add> ptr_specifier )* ;

ptr_specifier <CmmPtrSpecifier>:
    '*' <set pointer=true>
    <call> ptr_cv_specifier_list
  |
    '&' <set reference=true>
    <call> ptr_cv_specifier_list
  // |
  //   (type_name "::" '*') =>
  //   name:type_name "::" '*'
  //   <call> ptr_cv_specifier_list // !?
  ;

ptr_cv_specifier_list <modify CmmPtrSpecifier>:
  (
    "const" <set cv_const = true>
  |
    "volatile" <set cv_volatile = true>
  )* ;

/* function and array specifiers */

cont_specifier_list <CmmContSpecifierSect>:
  ( <add> cont_specifier )* ;

cont_specifier <select CmmContSpecifier>:
    function_specifier
  |
    array_specifier ;

function_specifier <CmmFunctionSpecifier:CmmContSpecifier>:
  '('
  parameters:parameter_declaration_list
  ')'
  (
    "const" <set cv_const = true>
  |
    "volatile" <set cv_volatile = true>
  )*
  ( exception_spec:exception_specification )? ;

array_specifier <CmmArraySpecifier:CmmContSpecifier>:
  '['
  (lim:expr)?
  ']' ;

/* parameter declarations */

parameter_declaration_list <CmmParamSect>:
  (
     <add> parameter_declaration
     ( ',' <add> parameter_declaration )*
  )? ;

parameter_declaration <CmmParamItem> <custom subst, list_subst>:
    type:common_type_id
    ( '=' init:assignment_expr )?
  |
     "..." <set dots=true> // !?
  ;

/* ---------------------------------------------------------------------- */

/* simple declaration */

simple_declaration <CmmSimpleDecl:CmmDecl> <custom subst, internal_send>:
  decl_spec:decl_specifiers
  type_spec:type_specifiers
  <add> simple_item
  (
     func_body:function_body
  |
     ( ',' <add> simple_item )*
     ';'
  );

simple_item <CmmSimpleItem>:
  decl:declarator
  // ( ':' width:const_expr )?
  ( init:initializer )?
  ;

/* local declaration head - only for predicate */

local_head <CmmLocalHead:CmmSimpleDecl> :
  decl_spec:decl_specifiers
  type_spec:type_specifiers
  decl:declarator
  ( '='  <set opt1=true> |
    ":=" <set opt2=true> |
    ','  <set opt3=true> |
    ';'  <set opt4=true> )
  ;

/* condition declaration */

condition_declaration <CmmSimpleDecl>:
  decl_spec:decl_specifiers
  type_spec:type_specifiers
  <add> simple_item
  ( ',' <add> simple_item )*
  // no semicolon
  ;

/* constructor declaration */

constructor_declaration <CmmCtorDecl:CmmSimpleDecl> <custom internal_send>:
  decl_spec:decl_specifiers
  <add> constructor_item
  (
     ( ':' <new_line> ctor_init:ctor_initializer )?
     func_body:function_body
  |
     ';'
  );

constructor_item <CmmSimpleItem>:
  ( '~' <set a_destructor = true> )?
  decl:declarator
  ;

/* constructor declaration head - only for predicate */

constructor_head <CmmCtorHead:CmmSimpleDecl> :
  decl_spec:decl_specifiers
  ( '~' <set a_destructor = true> )?
  name:qualified_name
  cont:cont_specifier_list
  ( ':' <set opt1=true> |
    '{' <set opt2=true> |
    ';' <set opt3=true> );

/* function body */

function_body <return CmmCompoundStat>:
  <new_line>
  compound_stat ;

/* ---------------------------------------------------------------------- */

// declarator          ... name or inner declarator
// abstract_declarator ... empty or inner declarator
// common_declarator   ... empty, name or inner declarator

/* ---------------------------------------------------------------------- */

/* declarator */

declarator <CmmDeclarator>:
  ptr:ptr_specifier_list
  (
     name:qualified_name
  |
     ( nested_declarator ) => // necessary
        '(' inner:declarator ')'
  )
  cont:cont_specifier_list
  ;

abstract_declarator <CmmDeclarator>:
  ptr:ptr_specifier_list
  (
     ( nested_declarator ) =>
        '(' inner:abstract_declarator ')'
  )?
  cont:cont_specifier_list
  ;

common_declarator <CmmDeclarator>:
  ptr:ptr_specifier_list
  (
     name:qualified_name
  |
     ( nested_declarator ) =>
        '(' inner:common_declarator ')'
  )?
  cont:cont_specifier_list
  ;

nested_declarator <CmmNestedDeclarator>:
  ( '(' <set something=true> )@
  ( '*' <set a_ptr = true> |
    '&' <set a_ref = true> );

/* ---------------------------------------------------------------------- */

// type_id ... used in expressions, initialization of template_type_param, template_type_arg, exception_specification
// common_type_id ... used in parameter_declaration, template_value_param, try handler

/* ---------------------------------------------------------------------- */

/* type id */

type_id <CmmTypeId>: /* abstract_type_id */
  type_spec:type_specifiers
  decl:abstract_declarator ;

common_type_id <CmmTypeId>:
  type_spec:type_specifiers
  decl:common_declarator ;

/* ---------------------------------------------------------------------- */

/* initializer */

initializer <CmmInitializer>:
      '=' value:initializer_item
   |
      ":=" '(' value2:expr_list ')' // !?
   ;

initializer_item <select CmmInitItem>:
     simple_initializer
   | initializer_list ;

simple_initializer <CmmInitSimple:CmmInitItem> <custom subst>:
  expr:assignment_expr ;

initializer_list <CmmInitList:CmmInitItem> <custom subst>:
  '{'
  <indent>
  (
    <add> initializer_item
    (
      ','
      <new_line>
      <add> initializer_item
    )*
  )?
  <unindent>
  '}';

/* ---------------------------------------------------------------------- */

/* enum */

enum_declaration <CmmEnumDecl:CmmDecl>:
  "enum"
  name:qualified_name
  (
     <new_line>
     '{'
        <indent>
        items:enum_list
        <unindent>
     '}'
   )?
   ';' ;

enum_list <CmmEnumSect>:
  (
    <add> enumerator
    ( ',' <add> enumerator )*
  )? ;

enumerator <CmmEnumItem>:
  <new_line>
  name:simple_name
  ( '=' init:const_expr )? ;

/* ---------------------------------------------------------------------- */

/* class */

class_declaration <CmmClassDecl:CmmDecl>:
  (
    "class"  <set style = ClassStyle> |
    "struct" <set style = StructStyle> |
    "union"  <set style = UnionStyle>
  )
  name:qualified_name
  (
     ( ':' base_list:base_specifier_list )?
     <new_line>
    '{'
        <indent>
        members:member_list
        <unindent>
    '}'
  )?
  ';' ;

/* members */

member_visibility <CmmMemberVisibility:CmmDecl> <custom internal_send>:
  ( "private"   <set access = PrivateAccess>
  | "protected" <set access = ProtectedAccess>
  | "public"    <set access = PublicAccess> )
  <no_space> ':' ;

member_item <select CmmDecl>:

    member_visibility

  | ( constructor_head ) => constructor_declaration
  | simple_declaration
  | empty_declaration

  | class_declaration
  | enum_declaration

  | typedef_declaration
  | friend_declaration

  | using_declaration
  | template_declaration ;

member_list <CmmDeclSect>:
  ( <add> member_item <new_line> )* ;

/* base specification */

base_specifier_list <CmmBaseSect>:
  <add> base_specifier
  ( ',' <add> base_specifier )* ;

base_specifier <CmmBaseItem>:
  ( "virtual" <set a_virtual = true> )?
  ( "private"   <set access = PrivateAccess>
  | "protected" <set access = ProtectedAccess>
  | "public"    <set access = PublicAccess>
  |             <set access = NoAccess> )
  from:qualified_name ;

/* constructor initializer */

ctor_initializer <CmmCtorInitializer>:
  <indent>
  <add> member_initializer
  (
     ','
     <new_line>
     <add> member_initializer
  )*
  <unindent> ;

member_initializer <CmmMemberInitializer>:
   name:qualified_name // !? qualified_name
   '('
   params:expr_list
   ')';

/* ---------------------------------------------------------------------- */

/* special member functions */

conversion_specifiers <return CmmTypeSpec>: // !?
  type_specifiers
  ptr:ptr_specifier_list ;

special_function <CmmSpecialFuction>:
   "operator"
   (
     conv:conversion_specifiers // !?

   | "new"    <set spec_new=true>    ( ('[' ']') => '[' ']' <set spec_new_array=true> )? // !?
   | "delete" <set spec_delete=true> ( ('[' ']') => '[' ']' <set spec_delete_array=true> )?

   | "+" <set spec_add=true>
   | "-" <set spec_sub=true>
   | "*" <set spec_mul=true>
   | "/" <set spec_div=true>
   | "%" <set spec_mod=true>
   | "^" <set spec_xor=true>
   | "&" <set spec_and=true>
   | "|" <set spec_or=true>
   | "~" <set spec_not=true>

   | "!"  <set spec_log_not=true>
   | "="  <set spec_assign=true>
   | "<"  <set spec_lt=true>
   | ">"  <set spec_gt=true>
   | "+=" <set spec_add_assign=true>
   | "-=" <set spec_sub_assign=true>
   | "*=" <set spec_mul_assign=true>
   | "/=" <set spec_div_assign=true>
   | "%=" <set spec_mod_assign=true>

   | "^="  <set spec_xor_assign=true>
   | "&="  <set spec_and_assign=true>
   | "|="  <set spec_or_assign=true>
   | "<<"  <set spec_shl=true>
   | ">>"  <set spec_shr=true>
   | ">>=" <set spec_shl_assign=true>
   | "<<=" <set spec_shr_assign=true>
   | "=="  <set spec_eq=true>
   | "!="  <set spec_ne=true>

   | "<="  <set spec_le=true>
   | ">="  <set spec_ge=true>
   | "&&"  <set spec_log_and=true>
   | "||"  <set spec_log_or=true>
   | "++"  <set spec_inc=true>
   | "--"  <set spec_dec=true>
   | ","   <set spec_comma=true>
   | "->*" <set spec_member_deref=true>
   | "->"  <set spec_deref=true>

   | "(" ")" <set spec_call=true>
   | "[" "]" <set spec_index=true>
   ) ;

/* ---------------------------------------------------------------------- */

/* template declaration */

template_declaration <CmmTemplateDecl:CmmDecl>:
  (
    "export"
    <set export_template = true>
  )?
  "template"
  (
    '<'
    params:template_param_list
    '>'
  )?
  inner_decl:declaration;

template_param_list <CmmTemplateParamSect>:
  (
    <add> template_param
    ( ',' <add> template_param )*
  )?;

template_param <select CmmTemplateParam>:
    ( "template" | "class" | "typename" ) => template_type_param
  |
    template_value_param
  ;

template_type_param <CmmTemplateTypeParam:CmmTemplateParam>:
  (
    "template" '<' params:template_param_list '>'
  )?
  (
    "class" <set a_class = true>
  |
    "typename" <set a_typename = true>
  )
  ( name:simple_name )?
  ( '=' init_type:type_id )?
  ;

template_value_param <CmmTemplateValueParam:CmmTemplateParam>:
  type:common_type_id
  ( '=' init_value:shift_expr )? ; // !?

/* template arguments */

template_arg_list <CmmTemplateArgSect>:
  '<'
  (
    <add> template_arg
    ( ',' <add> template_arg )*
  )?
  '>' ;

template_arg <select CmmTemplateArg>:
    (type_id) =>
      template_type_arg
  |
    template_value_arg;

template_type_arg <CmmTemplateTypeArg:CmmTemplateArg>:
  type:type_id; // !?

template_value_arg <CmmTemplateValueArg:CmmTemplateArg>:
   value:shift_expr; // !?

/* ---------------------------------------------------------------------- */

/* try statement */

try_stat <CmmTryStat:CmmStat>:
  "try"
  <new_line>
  body:compound_stat
  handlers:handler_list ;

handler_list <CmmHandlerSect>:
  <add> handler
  ( <add> handler )* ;

handler <CmmHandlerItem>:
  <new_line>
  "catch"
  '('
  (
    type:common_type_id
  |
    "..." <set dots=true>
  )
  ')'
  <new_line>
  body:compound_stat ;

/* throw expression */

throw_expr <CmmThrowExpr:CmmExpr>:
  "throw" (expr:assignment_expr)? ;

/* exception specification */

exception_specification <CmmExceptionSect>:
  "throw"
  '('
  (
    <add> exception_specification_item
    (',' <add> exception_specification_item)*
  )?
  ')' ;

exception_specification_item <CmmExceptionItem: CmmTypeDecl>:
  type:type_id ;

/* ---------------------------------------------------------------------- */
