
/* clang-debugger.h */

#ifndef CLANG_DEBUGGER_H
#define CLANG_DEBUGGER_H

#include <string>
using namespace std;

class Window;

void debug_with_lldb (Window * win, string fileName);

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all

#endif // CLANG_DEBUGGER_H
