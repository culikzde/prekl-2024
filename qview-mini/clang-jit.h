
/* clang-jit.h */

#ifndef CLANG_JIT_H
#define CLANG_JIT_H

#include <string>
#include <vector>

using std::string;
using std::vector;

typedef vector <string> string_list;

int compileAndRun (string_list options,
                   string_list libraries,
                   string_list new_argumets,
                   string_list new_environment);

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all

#endif // CLANG_JIT
