
/* clang-common.h */

#ifndef CLANG_COMMON_H
#define CLANG_COMMON_H

class QTreeWidget;
class QString;

void displayTranslationUnit (QTreeWidget * tree, QTreeWidget * classes, QString file_name);

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all

#endif // CLANG_COMMON_H
