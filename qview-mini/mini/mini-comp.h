
/* mini-comp.h */

#ifndef MINI_COMP_H
#define MINI_COMP_H

#include "code.h"
#include "mini-parser.hpp"

#include <string>
using namespace std;

SimScope * compile_program (string fileName);

#endif // MINI_COMP_H
