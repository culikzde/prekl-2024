
#ifndef MINI_INSTR_H
#define MINI_INSTR_H

class SimScope;

void instruction_test (SimScope * global_scope);

#endif // MINI_INSTR_H
