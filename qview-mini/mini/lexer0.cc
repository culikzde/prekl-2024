
/* lexer0.cc */

#include "lexer0.h"

#ifdef TRACE
   #include "trace.h"
#endif

#include <map>
#include <cassert>
#include <cstring>

#include <fstream>
#include <sstream>

using std::map;
using std::ifstream;
using std::ofstream;
using std::istringstream;
// using std::memcpy;
// using std::noskipws;
// using std::hex;
// using std::endl;
// using std::cerr;
// using std::streamsize;

/* ---------------------------------------------------------------------- */

inline void reset (ostringstream & storage)
{
   storage.str ("");
   storage.clear ();
}

/* ---------------------------------------------------------------------- */

inline bool isLetter (char ch)
{
   return (ch >= 'A' && ch <= 'Z') ||
          (ch >= 'a' && ch <= 'z') ||
          (ch == '_');
}

inline bool isUpperCaseLetter (char ch)
{
   return (ch >= 'A' && ch <= 'Z') || (ch == '_');
}

inline bool isDigit (char ch)
{
   return ch >= '0' && ch <= '9';
}

inline bool isOctal (char ch)
{
   return ch >= '0' && ch <= '7';
}

inline bool isHexDigit (char ch)
{
   return (ch >= '0' && ch <= '9') ||
          (ch >= 'a' && ch <= 'f') ||
          (ch >= 'A' && ch <= 'F');
}

inline bool isLetterOrDigit (int ch)
{
   return isLetter (ch) || isDigit (ch);
}

/* ---------------------------------------------------------------------- */

template <typename T>
inline string NumToStr (T num)
{
   ostringstream stream;
   stream << num;
   return stream.str ();
}
template <typename T>
inline void StrToNum (string txt, T & result, bool & ok)
// txt ... input value
// ok == true ... conversion succeded
// result ... converted value, assigned only when ok == true
{
   T n;
   istringstream stream (txt);
   stream >> std::noskipws;

   if (txt.length () > 2 && txt[0] == '0' && (txt[1] == 'x' || txt[1] == 'X'))
   {
      char c1;
      char c2;
      stream >> c1 >> c2 >> std::hex; // skip two characters and set hex
   }

   stream >> n;
   ok = (stream.eof () && ! stream.fail ());

   if (ok)
      result = n;

   // ok == false, nothing is assigned to result
}

template <typename T>
inline bool StrToNum (string txt, T & result)
{
   bool ok;
   StrToNum (txt, result, ok);
   return ok;
}

/* ---------------------------------------------------------------------- */

/* inline */ string IntToStr (int value)
// convert int to string
{
   return NumToStr (value);
}

inline void StrToInt (string txt, int & result, bool & ok)
// convert string to int
// txt ... input value
// ok == true ... conversion succeded
// result ... converted value, assigned only when ok == true
{
   StrToNum (txt, result, ok);
}

inline bool StrToInt (string txt, int & result)
// returns ok from previous function
{
   bool ok;
   StrToInt (txt, result, ok);
   return ok;
}

/* ---------------------------------------------------------------------- */

const char hex_digit [16+1] = "0123456789abcdef";

inline string charToStr (char c) { return string (1, c); } // convert one character to string

string quoteChrContent (char value)
{
   string result = "";

   unsigned char ch = value;

   if (ch < ' ' || ch == 128 || ch >= 255)
   {
      result = '\\';
      switch (ch)
      {
         case '\a': result = result + 'a'; break;
         case '\b': result = result + 'b'; break;
         case '\f': result = result + 'f'; break;
         case '\n': result = result + 'n'; break;
         case '\r': result = result + 'r'; break;
         case '\t': result = result + 't'; break;
         case '\v': result = result + 'v'; break;

         default:
            result = result + 'x' + hex_digit [ch >> 4] + hex_digit [ch & 15];
            break;
      }
   }
   else
   {
      if (ch == '\\' || ch == '"')
         result = charToStr ('\\') + value;
      else
         result = value;
   }

   return result;
}

string quoteStrContent (const string value)
{
   int len = value.length ();

   bool simple = true;
   for (int i = 0; i < len && simple; i++)
   {
      unsigned char ch = value [i];
      if (ch < ' ' || ch == '\\' || ch == '"' || ch == 128 || ch >= 255)
         simple = false;
   }

   if (simple)
   {
      return value;
   }
   else
   {
      string result = "";

      for (int i = 0; i < len; i++)
         result += quoteChrContent (value [i]);

      return result;
   }
}

string quoteStr (const string value, char quote)
{
   return quote + quoteStrContent (value) + quote;
}

string quoteChr (char value, char quote = quote1)
{
   return quote + quoteStrContent (charToStr (value)) + quote;
}

/* ---------------------------------------------------------------------- */

void show_message (MessageLevel level,
                   string msg, string file_name, int line, int column)
{
}

/******************************* FILE NAMES *******************************/

typedef map <string, int> StringMap;
typedef map <int, string> IntMap;

// global variables
static StringMap FileNameDict;
static IntMap FileNumberDict;
static int last_index = 0;

void clearFileNames ()
{
   FileNameDict.clear ();
   FileNumberDict.clear ();
}

int storeFileName (string file_name)
{
   int index = 0;

   if (FileNameDict.find (file_name) != FileNameDict.end ())
   {
      index = FileNameDict [file_name];
   }
   else
   {
      last_index ++;
      index = last_index;
      FileNameDict [file_name] = index;
      FileNumberDict [index] = file_name;
   }

   return index;
}

string recallFileName (int index)
{
   if (FileNumberDict.find (index) != FileNumberDict.end ())
   {
      return FileNumberDict [index];
   }
   else
   {
      return "";
   }

   // return empty string if file index is not dictionay
   // important for __FILE__ macro expansion
}

int recallFileIndex (string file_name)
{
   if (FileNameDict.find (file_name) != FileNameDict.end ())
   {
      return FileNameDict [file_name];
   }
   else
   {
      return 0;
   }
}

/********************************** LEXER *********************************/

Lexer::Lexer ():
   inpStream (NULL),
   bufPos (0),
   bufLen (0),
   // ignore buf
   releaseStream (false),

   fileInx (0),
   linePos (0),
   colPos (0),
   charPos (0),

   tokenFileInx (0),
   tokenLinePos (0),
   tokenColPos (0),
   tokenCharPos (0),

   ch (0),
   prevCh (0),
   prevEol (false),
   inpEof (false),

   positionInitialized (false),

   tokenKind (startToken),
   tokenText (""),
   tokenVal (""),
   tokenSym (0)
{
}

Lexer::~Lexer ()
{
   if (releaseStream && inpStream != NULL)
   {
      delete inpStream;
      inpStream = NULL;
   }
}

void Lexer::Init ()
{
   bufPos = 0;
   bufLen = 0;
   // do not change already initialized inpStream, fileName and releaseStream

   ch = 0;
   prevCh = 0;
   prevEol = false;
   inpEof = false;

   if (! positionInitialized)
   {
      linePos = 1;
      colPos = 0;
      charPos = 0;

      tokenLinePos = 1;
      tokenColPos = 0;
      tokenCharPos = 0;
   }

   tokenKind = startToken;
   tokenText = "";
   tokenVal = "";
   tokenSym = 0;
   // actual = NO_SYMBOL;

   // read first character
   nextCh ();
   nextToken ();
}

string Lexer::getFileName ()
{
   return recallFileName (fileInx);
}

void Lexer::setFileName (const string file_name)
{
   fileInx = storeFileName (file_name);
}

/* ---------------------------------------------------------------------- */

void Lexer::openStream (istream & stream)
{
   inpStream = & stream;
   assert (inpStream != NULL);
   Init ();
}

void Lexer::openString (string text)
{
   istringstream * tmp = new istringstream (text);
   releaseStream = true;
   openStream (*tmp);
}

void Lexer::open (string name)
{
   setFileName (name);

   ifstream * tmp = new ifstream;
   tmp->open (name.c_str ());

   if (!tmp->good ())
   {
      error ("Cannot open " + name);
      delete tmp;
   }
   else
   {
      releaseStream = true;
      openStream (*tmp);
   }
}

/* ---------------------------------------------------------------------- */

void Lexer::readBuf ()
{
   assert (inpStream != NULL);
   if (! inpStream->good ()) // important for string streams
   {
       bufLen = 0;
   }
   else
   {
      inpStream->read (static_cast <char *> (static_cast <void *> (& buf)), bufSize);
      bufLen = inpStream->gcount ();
   }
   bufPos = 0;
}

void Lexer::readData (void * adr, size_t size)
{
   size_t step;
   assert (bufPos >= 0);
   assert (bufPos <= bufLen);

   assert (bufLen >= 0);
   assert (bufLen <= bufSize);

   while (size > 0)
   {
      if (bufPos >= bufLen)
      {
         readBuf ();
         if (bufLen == 0)
            error ("End of file");
      }

      step = bufLen - bufPos;
      if (step > size)
         step = size;

      assert (step > 0);
      assert (step <= size);

      assert (bufPos >= 0);
      assert (bufPos + step <= bufLen);

      assert (bufLen >= 0);
      assert (bufLen <= bufSize);

      std::memcpy (adr, & buf[bufPos], step);
      bufPos += step;
      size -= step;

      adr = (void *) ((char *) (adr) + step);

      assert (size >= 0);
   }
}

void Lexer::getCh ()
{
   // assert (bufPos >= 0);
   // assert (bufPos <= bufLen);

   // assert (bufLen >= 0);
   // assert (bufLen <= bufSize);

   if (bufPos >= bufLen)
   {
      if (inpEof)
      {
          error ("End of file");
      }
      else
      {
         readBuf ();
         inpEof = (bufLen == 0);
      }
   }

   if (inpEof)
   {
      ch = 0;
   }
   else
   {
      // assert (bufPos >= 0);
      // assert (bufPos < bufLen);

      // assert (bufLen >= 0);
      // assert (bufLen <= bufSize);

      ch = (char) (buf[bufPos]);
      bufPos ++;
   }

   // assert (bufPos >= 0);
   // assert (bufPos <= bufLen);

   // assert (bufLen >= 0);
   // assert (bufLen <= bufSize);

   if (prevEol)
   {
      linePos ++;
      colPos = 0;
      prevEol = false;
   }

   if (ch == cr)
   {
      prevEol = true;
      colPos ++;
   }
   else if (ch == lf)
   {
      if (prevCh != cr)
      {
         prevEol = true;
         colPos ++;
     }
   }
   else
   {
      colPos ++;
   }

   charPos ++;
   prevCh = ch;
}

/* ---------------------------------------------------------------------- */

void Lexer::message (MessageLevel level, string msg)
{
   string file_name = getFileName ();
   int line = getTokenLine();
   int column = getTokenCol();

   // show position

   string txt = "";

   if (file_name != "")
   {
      txt = file_name + ":";
      if (line != 0) // !?
         txt = txt + IntToStr (line) + ":";
   }
   else
   {
      if (line != 0)
         txt = "line " + IntToStr (line) + ":";
   }

   if (column != 0)
      txt = txt + IntToStr (column) + ":";
      // emacs - column number, without any space, with colon

   // show message

   if (txt != "")
      txt = txt + " ";

   switch (level)
   {
      case DebugLevel:
         txt = txt + "debug: ";
         break;

      case InfoLevel:
         txt = txt + "info: ";
         break;

      case WarningLevel:
         txt = txt + "warning: ";
         break;

      case ErrorLevel:
         txt = txt + "error: ";
         break;
   }

   txt = txt + msg;

   txt = txt + ", tokenKind = " + std::to_string (tokenKind);
   txt = txt + ", tokenText = " + tokenText;

   std::cout << txt << std::endl;

   if (level == ErrorLevel)
   {

      #ifdef TRACE
         trace (); // show stack trace (before throwing exception)
      #endif

      throw LexerException (msg, file_name, line, column);
   }
}

void Lexer::debug (const string msg)
{
   message (InfoLevel, msg);
}

void Lexer::info (const string msg)
{
   message (InfoLevel, msg);
}

void Lexer::warning (const string msg)
{
   message (WarningLevel, msg);
}

void Lexer::error (const string msg)
{
   message (ErrorLevel, msg);
}

/******************************* TEXT INPUT *******************************/

void Lexer::nextCh ()
{
   // NO value_storage.put (inpCh); // token characters
   text_storage.put (ch); // characters from original source
   getCh ();
}

void Lexer::textCh ()
// characters in string literals
{
   // NO value_storage.put (inpCh);
   text_storage.put (ch);

   getCh ();
}

/* ---------------------------------------------------------------------- */

void Lexer::getIdent ()
{
   while (isLetterOrDigit (ch))
      nextCh ();

   tokenText = text_storage.str ();
   tokenVal = "";
   tokenSym = 0;
   tokenKind = identToken;
}

void Lexer::getNumber ()
{
   bool hex = false;
   bool any_digit = false;
   bool floating_point = false;

   if (ch == '0')
   {
      nextCh (); // first digit
      any_digit = true;

      if (ch == 'x' || ch == 'X')
      {
         nextCh (); // store 'x'
         hex = true;
         while (isHexDigit (ch))
            nextCh ();
      }
   }

   if (! hex)
   {
      while (isDigit (ch))
      {
         any_digit = true;
         nextCh ();
      }

      if (ch == '.')
      {
         nextCh (); // decimal point
         floating_point = true;

         while (isDigit (ch))
         {
            any_digit = true;
            nextCh ();
         }
      }

      if (any_digit && (ch == 'E' || ch == 'e'))
      {
         nextCh ();
         floating_point = true;

         if (ch == '+' || ch == '-')
            nextCh ();

         if (! isDigit (ch))
            error ("Digit expected");

         while (isDigit (ch))
            nextCh ();
      }
   }

   if (any_digit)
   {
      if (floating_point)
         while (ch == 'f' || ch == 'F' || ch == 'l' || ch == 'L')
            nextCh ();
      else
         while (ch == 'u' || ch == 'U' || ch == 'l' || ch == 'L')
            nextCh ();
   }

   if (any_digit)
   {
      tokenText = text_storage.str ();
      tokenVal = "";
      tokenSym = 0;

      tokenKind = numberToken;
   }
   else
   {
      storeSeparator ('.');
   }
}

char Lexer::character ()
// read one C-style character
{
  if (ch != backslash)             // simple character
  {
     char last = ch;
     textCh ();
     return last;
  }
  else                                // escape sequence
  {
     textCh ();                       // skip backslash

     if (isOctal (ch))             // octal
     {
        int n = 0;
        int cnt = 1;
        while (isOctal (ch) && cnt <= 3)
        {
           n = n*8 + ch-'0';
           cnt++;
           textCh ();
        }
        return char (n);
     }

     else if (ch == 'x' || ch == 'X')     // hex
     {
        textCh ();
        int n = 0;
        while (isHexDigit (ch))
        {
           int d;
           if (ch >= 'A' && ch <= 'F')
              d = ch - 'A' + 10;
           else if (ch >= 'a' && ch <= 'f')
              d = ch - 'a' + 10;
           else
              d = ch - '0';
           n = n*16 + d;
           textCh ();
        }
        return char (n);
     }
     else
     {
        char last = ch;
        textCh ();
        switch (last)                 // other
        {
           case 'a': return '\a';
           case 'b': return '\b';
           case 'f': return '\f';
           case 'n': return '\n';
           case 'r': return '\r';
           case 't': return '\t';
           case 'v': return '\v';

           case quote1:    return last;
           case quote2:    return last;
           case backslash: return last;
           case '?':       return last;

           default:        return last;
        }
     }
  }
}

void Lexer::getChar ()
{
   textCh (); // skip quote

   // if (inpCh == quote1)
   //    error ("Empty character constant");

   while (ch != quote1)
   {
      if (inpEof || ch == cr || ch == lf)
         error ("character constant exceeds line");

      char c;

      c = character ();

      value_storage.put (c);
   }

   textCh (); // skip quote

   tokenText = text_storage.str (); // with quotes and escape sequences
   tokenVal = value_storage.str (); // value without quotes and escape sequences
   tokenSym = 0;
   tokenKind = charToken;
}

void Lexer::getString ()
{
   textCh (); // skip quote

   while (ch != quote2)
   {
      if (inpEof || ch == cr || ch == lf)
         error ("String exceeds line");

      char c;

      c = character ();

      value_storage.put (c);
   }

   textCh (); // skip quote

   tokenText = text_storage.str (); // string with quotes and escape sequences
   tokenVal = value_storage.str (); // value without quotes and escape sequences
   tokenSym = 0;
   tokenKind = stringToken;
}

/* ---------------------------------------------------------------------- */

void Lexer::storeSeparator (char ch)
{
   text_storage.put (ch);

   tokenText = ch;
   tokenVal = "";
   tokenSym = ch; // important
   tokenKind = separatorToken;
 }

void Lexer::getSeparator ()
{
   char c = ch;
   nextCh ();
   storeSeparator (c);
}

/* ---------------------------------------------------------------------- */

void Lexer::nextToken ()
{
   reset (text_storage);
   reset (value_storage);

   again:

   tokenFileInx = fileInx;
   tokenLinePos = linePos;
   tokenColPos  = colPos;
   tokenCharPos = charPos;

   if (inpEof)                                 // END OF FILE
   {
      tokenText = "";
      tokenVal = "";
      tokenSym = 0;
      tokenKind = eofToken;
   }

   else if (ch <= ' ')                      // WHITE SPACE, END OF LINE
   {
      while (! inpEof && ch <= ' ')
         getCh ();

      goto again;
   }

   else if (ch == '/')                      // COMMENT (or slash)
   {
      bool slash = false;
      getCh (); // skip slash

      if (! inpEof && ch == '/')            // SINGLE LINE COMMENT
      {
         while (! inpEof && ch != cr && ch != lf)
            getCh ();
      }

      else if (! inpEof && ch == '*')       // COMMENT
      {
         getCh (); // skip asterisk

         char prev = ' ';
         while (! inpEof && ! (prev == '*' && ch == '/'))
         {
            prev = ch;
            getCh ();
         }

         if (! inpEof)
            getCh (); // skip slash
      }
      else slash = true; // SLASH

      if (slash)
         storeSeparator ('/');
      else
         goto again;
   }

   else if (isLetter (ch))                  // IDENTIFIER
      getIdent ();

   else if (isDigit (ch))                   // NUMBER
      getNumber ();

   else if (ch == '.')                      // NUMBER or PERIOD
      getNumber ();

   else if (ch == quote1)                   // CHARACTER
      getChar ();

   else if (ch == quote2)                   // STRING
      getString ();

   else                                        // SEPARATOR
      getSeparator ();

   translate (); // convert keywords and separators
}


/* ---------------------------------------------------------------------- */

bool Lexer::isIdent (string par)
{
   return (tokenKind == identToken) && (tokenText == par);
}

bool Lexer::isSeparator (char par)
{
   return (tokenKind == separatorToken) && (tokenSym == par);
}

/* ---------------------------------------------------------------------- */

void Lexer::checkIdent (string par)
{
   if (tokenKind != identToken || tokenText != par)
      // error (par + " expected");
      error (par + " expected and found " + quoteStr (tokenText));
   nextToken ();
}

void Lexer::checkSeparator (char par)
{
   if (tokenKind != separatorToken || tokenSym != par)
   {
      // error (charToStr (par) + " expected");
      error (charToStr (par) + " expected and found " + quoteStr (tokenText));
   }
   nextToken ();
}

void Lexer::checkEof ()
{
   if (tokenKind != eofToken)
      error ("End of file expected");
}

/* ---------------------------------------------------------------------- */

string Lexer::readIdentifier ()
{
   string result;
   if (tokenKind != identToken)
      error ("Identifier expected");
   result = tokenText;
   nextToken ();
   return result;
}

string Lexer::readNumber ()
{
   string result;
   if (tokenKind != numberToken)
      error ("Number expected");
   result = tokenText;
   nextToken ();
   return result;
}

char Lexer::readChar ()
{
   char result;
   if (tokenKind != charToken)
      error ("character constant expected");
   result = tokenSym;
   nextToken ();
   return result;
}

string Lexer::readString ()
{
   string result;
   if (tokenKind != stringToken)
      error ("String expected");
   result = tokenVal;
   nextToken ();
   return result;
}

/* ---------------------------------------------------------------------- */

bool Lexer::readBool ()
{
   bool result = false;

   if (isIdent ("true"))
   {
      nextToken ();
      result = true;
   }
   else if (isIdent ("false"))
   {
      nextToken ();
      result = false;
   }
   else
   {
      error ("Boolean value expected");
   }

   return result;
}

string Lexer::readSignedNumber ()
{
   bool minus;
   string result;

   minus = false;
   if (tokenKind == separatorToken && tokenSym == '-')
   {
      minus = true;
      nextToken ();
   }

   result = readNumber ();

   if (minus)
      result = '-' + result;
   return result;
}

string Lexer::readValue ()
{
   string result;

   if (isSeparator ('-') || tokenKind == numberToken)
   {
      result = readSignedNumber ();
   }
   else if (tokenKind == identToken)
   {
      result = readIdentifier ();
      while (isSeparator ('.'))
      {
          nextToken ();
          string id = readIdentifier ();
          result = result + "." + id;
      }
   }
   else if  (tokenKind == stringToken || tokenKind == charToken )
   {
      result = tokenVal;
      nextToken ();
   }
   else
   {
      error ("Identifier, number, character or string expected");
   }

   return result;
}

/* ---------------------------------------------------------------------- */

int Lexer::readInt ()
{
   int result = 0;
   string txt = readSignedNumber ();
   bool ok;
   StrToNum (txt, result, ok);
   if (!ok)
      error ("Integer expected");
   return result;
}

long Lexer::readLong ()
{
   long result = 0;
   string txt = readSignedNumber ();
   bool ok;
   StrToNum (txt, result, ok);
   if (!ok)
      error ("Long integer expected");
   return result;
}

unsigned int Lexer::readUInt ()
{
   unsigned int result = 0;
   string txt = readSignedNumber ();
   bool ok;
   StrToNum (txt, result, ok);
   if (!ok)
      error ("Unsigned integer expected");
   return result;
}

unsigned long Lexer::readULong ()
{
   unsigned long result = 0;
   string txt = readSignedNumber ();
   bool ok;
   StrToNum (txt, result, ok);
   if (!ok)
      error ("Unsinged long integer expected");
   return result;
}

float Lexer::readFloat ()
{
   float result = 0;
   string txt = readSignedNumber ();
   bool ok;
   StrToNum (txt, result, ok);
   if (!ok)
      error ("Float expected");
   return result;
}

double Lexer::readDouble ()
{
   double result = 0;
   string txt = readSignedNumber ();
   bool ok;
   StrToNum (txt, result, ok);
   if (!ok)
      error ("Double expected");
   return result;
}

/****************************** TEXT OUTPUT *******************************/

Output::Output ():
   text_storage (),
   fileName (),
   showMsg (true),
   startLine (false),
   skipEmptyLine (false),
   addEmptyLine (false),
   ignoreEol (false),
   indentation (0),
   linePos (0),
   colPos (0)
{
}

Output::~Output ()
{
   // NO store ();
}

void Output::Init ()
{
   reset (text_storage); // important for repeated open

   indentation = 0;
   startLine = true;
   skipEmptyLine = false;
   addEmptyLine = false;
   ignoreEol = false;

   linePos = 1;
   colPos = 0;
}

void Output::open (string name)
{
   setFileName (name);
   Init ();
}

/* ---------------------------------------------------------------------- */

void Output::message (MessageLevel level, const string msg)
{
   show_message (level, msg, fileName, linePos, colPos);
}

/* ---------------------------------------------------------------------- */

void Output::debug (const string msg)
{
   message (InfoLevel, msg);
}

void Output::info (const string msg)
{
   message (InfoLevel, msg);
}

void Output::warning (const string msg)
{
   message (WarningLevel, msg);
}

void Output::error (const string msg)
{
   message (ErrorLevel, msg);
}

/* ---------------------------------------------------------------------- */

void Output::setIndent (int i)
{
   indentation = i;
}

void Output::indent ()
{
   indentation += 3;
}

void Output::unindent ()
{
   indentation -= 3;
}

/* ---------------------------------------------------------------------- */

void Output::putPlainChr (char c)
{
   text_storage.put (c);

   if (c == lf)
   {
      linePos ++;
      colPos = 0;
   }
   else
   {
      colPos ++;
   }
}

void Output::putPlainSpaces (int n)
{
   text_storage << string (n, ' ');
   colPos += n;
}

void Output::putPlainStr (const string s)
{
   text_storage << s;
   colPos += s.length (); // no line feed checking
}

void Output::putPlainData (void * adr, size_t len)
{
   text_storage.write ((const char *) adr, len);
   // no line and column numbers
}

void Output::putPlainEol ()
{
   // putPlainChr (cr);
   putPlainChr (lf);
}

/* ---------------------------------------------------------------------- */

void Output::openLine ()
{
   if (addEmptyLine && ! skipEmptyLine)
      putPlainEol ();

   addEmptyLine = false;
   skipEmptyLine = false;

   putPlainSpaces (indentation);
   startLine = false;

   addSpace = false;
   skipSpace = false;
}

void Output::closeLine ()
{
   startLine = true;
}

/* ---------------------------------------------------------------------- */

void Output::put (const string s)
{
   if (s != "")
   {
      if (startLine)
         openLine ();

      putPlainStr (s);
   }
}

void Output::putChr (char c)
{
   if (startLine)
      openLine ();

   putPlainChr (c);
}

void Output::putSpaces (int n)
{
   if (n != 0)
   {
      if (startLine)
         openLine ();

      putPlainSpaces (n);
  }
}

void Output::putEol ()
{
   if (ignoreEol)
   {
      putPlainChr (' ');
   }
   else
   {
      putPlainEol ();
      closeLine ();
   }
}

void Output::putCondEol ()
{
   // finish not empty line
   if (! startLine)
      putEol ();
}

void Output::putLn (const string s)
{
   put (s);
   putEol ();
}

/* ---------------------------------------------------------------------- */

void Output::emptyLine ()
{
   if (! startLine)
      putEol ();

   addEmptyLine = true;
}

void Output::noEmptyLine ()
{
   skipEmptyLine = true;
}

/* ---------------------------------------------------------------------- */

void Output::send (string s)
{
   // add optional space and print string

   char c = ' ';
   if (s.length () >= 1) c = s[0];

   if (c == ',' || c == ';' || c == ')' || c == ']' ) addSpace = false;

   if (addSpace && ! skipSpace && ! startLine) put (" ");
   put (s);

   addSpace = true;
   skipSpace = false;
   if (c == '(' || c == '[') addSpace = false;
}

void Output::sendeol ()
{
   putEol ();
}

void Output::sendln (string s)
{
   send (s);
   sendeol ();
}

/* ---------------------------------------------------------------------- */

void Output::style_space ()
{
   addSpace = true;
}

void Output::style_no_space ()
{
   skipSpace = true;
}

/* ---------------------------------------------------------------------- */

void Output::style_new_line ()
{
   // finish line and start new one
   putCondEol ();
}

void Output::style_empty_line ()
{
   // add one empty line
   emptyLine ();
}

void Output::style_no_empty_line ()
{
   // ignore style_empty_line ()
   noEmptyLine ();
}

/* ---------------------------------------------------------------------- */

void Output::style_indent ()
{
   // indent recomendation
   style_new_line ();
   indent ();
}

void Output::style_unindent ()
{
   unindent ();
   style_new_line ();
}

/* ---------------------------------------------------------------------- */

void Output::putBool (bool value)
{
   if (value)
      put ("true");
   else
      put ("false");
}

void Output::putChar (char value)
{
    put (quoteChr (value, quote1));
}

void Output::putString (string value)
{
    put (quoteStr (value, quote2));
}

void Output::putInt (int value)
{
    put (NumToStr (value));
}

void Output::putLong (long value)
{
    put (NumToStr (value));
}

void Output::putUInt (unsigned int value)
{
    put (NumToStr (value));
}

void Output::putULong (unsigned long value)
{
    put (NumToStr (value));
}

void Output::putFloat (float value)
{
    put (NumToStr (value));
}

void Output::putDouble (double value)
{
    put (NumToStr (value));
}
/* ---------------------------------------------------------------------- */

/* ---------------------------------------------------------------------- */

/* ---------------------------------------------------------------------- */

string Output::toString ()
{
   return text_storage.str ();
}

/* ---------------------------------------------------------------------- */

void Output::writeText ()
{
   ofstream f (fileName.c_str ());
   if (! f.good ())
   {
      error ("Cannot open file for writing: " + fileName);
   }
   else
   {
      string text = text_storage.str ();
      f << text;

      f.close ();
      if (! f.good ())
         error ("Error writing file: " + fileName); // simple error form inf.h
   }
}

/* ---------------------------------------------------------------------- */

bool Output::compare ()
{
   bool equal = false;

   ifstream f (fileName.c_str ());
   if (f.good ())
   {
      ostringstream buffer;
      buffer << f.rdbuf();
      string file_text = buffer.str ();

      string local_text = text_storage.str ();
      equal = file_text == local_text;
      f.close ();
   }

   return equal;
}

/* ---------------------------------------------------------------------- */

void Output::store ()
{
   bool equal = compare ();

   if (! equal)
   {
      writeText ();
      info ("writing " + fileName);
   }
   else
   {
      if (showMsg)
         info ("not modified " + fileName);
   }
}

/* ---------------------------------------------------------------------- */
