
QT          += widgets uiplugin designer dbus

CONFIG      += plugin

TEMPLATE    = lib

HEADERS     = connectionwidget.h \
              connectionplugin.h

SOURCES     = connectionwidget.cc \
              connectionplugin.cc

# target.path = $$[QT_INSTALL_PLUGINS]/designer
target.path = _output/plugins/designer
INSTALLS += target
