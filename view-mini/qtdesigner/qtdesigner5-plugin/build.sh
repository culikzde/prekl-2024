#!/bin/sh

QMAKE=qmake-qt5
which $QMAKE || QMAKE=qmake

test -f Makefile && make distclean
mkdir -p _output/plugins/designer
$QMAKE connectionplugin.pro || exit 1
make || exit 1
make install || exit 1
# sh designer.sh

# dnf install python3-sip-devel
# dnf install python3-qt5-devel
# dnf install qt5-qttools-devel
# dnf install qt5-designer

# apt-get install pyqt5-dev python3-sip-dev qttools5-dev

# pacman -S sip4 python-sip4 python-pyqt5 python-pyqt5-sip pkgconf
# sip4 and python-sip4 now important
