#include "example.h"
#include "ui_example.h"

#include <QApplication>

MainWindow::MainWindow (QWidget *parent)
    : QMainWindow (parent)
    , ui (new Ui::MainWindow)
{
    ui->setupUi (this);
}

MainWindow::~MainWindow ()
{
    delete ui;
}


int main (int argc, char *argv[])
{
    QApplication a (argc, argv);
    MainWindow w;
    w.show ();
    return a.exec ();
}

void MainWindow::on_firstButton_clicked()
{

}

void MainWindow::on_firstButton_pressed()
{

}

void MainWindow::on_firstButton_released()
{

}

void MainWindow::on_firstButton_toggled(bool checked)
{

}
