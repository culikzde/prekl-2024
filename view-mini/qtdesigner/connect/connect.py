
from __future__ import print_function

import os, sys

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

from PyQt5.QtDesigner import *

from clang.cindex import Index, Config, CursorKind, TypeKind, TranslationUnit, AccessSpecifier

import re, subprocess # gcc_options

# --------------------------------------------------------------------------

use_qt4 = False
use_qt6 = False
use_python2 = sys.version_info < (3,)
use_py2_qt4 = use_python2 and use_qt4

def qstring_to_str (s) :
    if use_py2_qt4 :
       if isinstance (s, QString) :
          return str (s.toAscii())
       else :
          return str (s)
    else :
       return s

def bytearray_to_str (b) :
    if use_python2 :
       return str (b)
    else :
       return str (b, "ascii")

def menu_exec (menu, point) :
    if use_qt6 :
       # menu.exec () # python 2 => syntax error
       return getattr (menu, "exec") (point)
    else :
       return menu.exec_ (point)

if use_python2 :
   def conv (s) :
       return str (s)
else :
   def conv (s) :
       return str (s, "utf-8")

# --------------------------------------------------------------------------

def gcc_options (gcc = "gcc", complete = False, selected_variables = False, no_variables = False) :

    cmd = "echo 'int main () { }' | " + gcc + " -v -x c++ -dD -E - 2>&1"

    result = [ ]

    f = subprocess.Popen (cmd, shell=True, stdout=subprocess.PIPE).stdout
    incl = False

    for s in f :
      s = s.strip ()
      s = conv (s)
      if re.match ("End of search list.", s) :
         incl = False
      if incl and s != "" :
         # s = os.path.normpath (s)
         # result.append ("-I " + s)
         result.append ("-I")
         result.append (s)
         # two items    "-I", "directory",
         # one item     "-Idirectory"
         # NOT one item "-I directory"
      # if re.match ("#include \"...\" search starts here:", s) :
      #    incl = True
      if re.match ("#include <...> search starts here:", s) :
         incl = True

    f.close ()
    return result

def pkg_options (pkg, libs = False) :
    cmd = "pkg-config " + pkg + " --cflags"
    if libs :
       cmd = cmd + " --libs"
    f = subprocess.Popen (cmd, shell=True, stdout=subprocess.PIPE).stdout
    result = [ ]
    for s in f :
       s = s.strip ()
       s = conv (s)
       for t in s.split () :
          result.append (t)
    return result

# --------------------------------------------------------------------------

class Connect (QTreeWidget):

    def __init__ (self):
        super (Connect, self).__init__ (None)

        self.translation_unit = None
        self.translation_index = None

        self.setHeaderHidden (True)
        # self.connect (self.tree_clicked)

        self.setContextMenuPolicy (Qt.CustomContextMenu)
        self.customContextMenuRequested.connect (self.onContextMenu)

    def translate (self, args) :
        # print ("ARGS:", args)
        index = Index.create ()
        tu = index.parse (None, args)
        if tu :
           for item in tu.diagnostics :
               print (item)
           self.translation_index = index # important, keep index
           self.translation_unit = tu
           self.add_item (self, tu.cursor)
           # for t in tu.cursor.get_children () :
               # self.add_item (self, t)

    def add_item (self, above, cursor) :
        location = cursor.location
        if location.file == None or location.file.name.startswith ("input/") :

           txt = str (cursor.spelling)
           txt = str (cursor.kind).replace ("CursorKind.", "", 1) + ": " + txt

           start = cursor.extent.start
           stop = cursor.extent.end
           # txt = txt + " : " + str (location)
           txt = txt + " from " + str (start) + " to " + str (stop)

           node = QTreeWidgetItem (above)
           node.setText (0, txt)
           node.cursor = cursor

           if ( cursor.kind == CursorKind.TRANSLATION_UNIT or
                cursor.kind == CursorKind.NAMESPACE or
                cursor.kind == CursorKind.CLASS_DECL ) :
              for t in cursor.get_children () :
                  self.add_item (node, t)

    def ui (self, fileName) :
        loader = QFormBuilder ()
        f = QFile (fileName)
        f.open (QFile.ReadOnly)
        widget = loader.load (f)
        f.close()

        self.ui_branch (self, widget)

    def ui_branch (self, above, widget) :
        title = widget.objectName ()
        title = qstring_to_str (title)
        if title != "" and not title.startswith ("qt_") :

           node = QTreeWidgetItem (above)
           node.setText (0, title)
           node.obj = widget

           for child in widget.children () :
              self.ui_branch (node, child)

    def onContextMenu (self, pos) :
        node = self.itemAt (pos)
        widget = getattr (node, "obj", None)
        if widget != None :
           menu = QMenu (self)
           cnt = 0

           typ = widget.metaObject ()
           cnt = typ.methodCount ()

           for inx in range (cnt) :
               method = typ.method (inx)
               if method.methodType () == QMetaMethod.Signal :
                  if use_qt4 :
                     func = qstring_to_str (method.signature ())
                  else :
                     func = bytearray_to_str (method.methodSignature ())
                  act = menu.addAction (func)
                  act.triggered.connect (lambda param, self=self, widget=widget, func=func: self.onSignal (widget, func))
                  cnt = cnt + 1

           if cnt > 0 :
              menu_exec (menu, self.mapToGlobal (QPoint (pos)))

    def onSignal (self, widget, signature) :
        self.add_declaration (widget, signature)
        self.add_implementation (widget, signature)

    def add_declaration (self, widget, signature) :
        cursor = self.translation_unit.cursor
        any = False
        for t in cursor.get_children () :
              if t.kind == CursorKind.CLASS_DECL :
                 if t.location.file.name.startswith ("input/") :
                    if not str (t.spelling).startswith ("Ui_") :
                       cls = t
                       print ("class", cls.spelling)
                       any = True
        if any :
           print ("CLASS", cls.spelling)
           there = False
           any_method = False
           for t in cls.get_children () :
              if t.kind == CursorKind.CXX_ACCESS_SPEC_DECL :
                 acs = t.access_specifier
                 there = acs == AccessSpecifier.PUBLIC
              if there :
                 if t.kind == CursorKind.CXX_METHOD :
                    method = t
                    any_method = True
           if any_method :
              stop = method.extent.end
              print ("stop", stop)
              code = "\n" + "void " + "on_" + widget.objectName () + "_" + signature + "\n"
              print ("CODE", code)
              pos = stop.offset
              fileName = str (stop.file)
              with open (fileName, "r+") as f :
                 text = f.readlines ()
                 text.insert (pos, code)
                 f.seek (0)
                 f.writelines (text)

    def add_implementation (self, widget, signature) :
        cursor = self.translation_unit.cursor
        any = False
        for t in cursor.get_children () :
              last = t
              any = True
        if any :
           stop = last.extent.end
           code = "\n" + "void " + "on_" + widget.objectName () + "_" + signature + "\n" + "{ }\n"
           print ("CODE", code)
           pos = stop.offset
           fileName = str (stop.file)
           with open (fileName, "r+") as f :
              text = f.readlines ()
              text.insert (pos, code)
              f.seek (0)
              f.writelines (text)


# --------------------------------------------------------------------------

if __name__ == "__main__" :
    app = QApplication (sys.argv)
    win = Connect ()
    win.show ()

    opts = [ "input/example.cc" ]
    # opts = opts + [ "-I", "input" ]
    opts = opts + gcc_options ("clang")
    opts = opts + pkg_options ("Qt5Widgets")
    win.translate (opts)

    win.ui ("input/example.ui")

    sys.exit (app.exec_ ())

# --------------------------------------------------------------------------

# dnf install python3-clang python3-qt5

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
