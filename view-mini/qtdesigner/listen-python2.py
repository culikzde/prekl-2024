#! /usr/bin/env python

from __future__ import print_function

import gobject
import dbus
import dbus.service
import dbus.mainloop.glib

class SomeObject(dbus.service.Object):

    #@dbus.service.method ("org.example.ReceiverInterface",
                          #in_signature='s',
                          #out_signature='s')
    @dbus.service.method ("org.example.ReceiverInterface")
    def hello (self, hello_message) :
        print ("Hello called with parameter:", str (hello_message))
        return "Hello from server (" +   hello_message + ")"

    @dbus.service.method ("org.example.ReceiverInterface")
    def navigateToSlot (self, objectName, signalSignature, parameterNames) :
        names =  [ str (item) for item in parameterNames ]
        print ("navigateToSlot", objectName, signalSignature, str (names))
        return None

if __name__ == '__main__':
    dbus.mainloop.glib.DBusGMainLoop (set_as_default=True)

    session_bus = dbus.SessionBus ()
    name = dbus.service.BusName ("org.example.receiver", session_bus)
    object = SomeObject (session_bus, '/org/example/ReceiverObject')

    mainloop = gobject.MainLoop ()
    print ("Running example service.")
    mainloop.run ()

# Debian 9: apt-get install python-dbus
