#!/bin/sh

sip-build --verbose --target-dir=. --no-make || exit 1

cd _build && make && cd .. || exit 1
cp _build/designer/libdesigner.so designer.so || exit 1

python run.py

# pacman -S sip (python-pyqt5-sip) python-pyqt5 pyqt-builder
# conflict with sip4 python-sip4

# dnf install python3-qt5-devel PyQt-builder (sip6) python3-devel qt5-qttools-devel

# Fedora 36: see python3-poppler-qt5- ... .fc36.src.rpm
# Archlinux: see python-poppler-qt5, http://github.com/frescobaldi/python-poppler-qt5/blob/master/pyproject.toml

# https://www.riverbankcomputing.com/static/Docs/PyQt-builder/pyproject_toml.html

