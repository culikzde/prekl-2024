
# cmm_generate.py

from __future__ import print_function

try :
   # http://github.com/Maratyszcza/PeachPy/archive/refs/heads/master.zip
   # dnf install python2 python2-setuptools
   # cd _library/PeachPy-master
   # python2 setup.py generate

   from peachpy import *
   from peachpy.x86_64 import *
   from peachpy.name import *
   from peachpy.x86_64.abi import *

   use_peachpy = True
except :
   print ("missing peachpy")
   use_peachpy = False

from code import *
from cmm_parser import *

# clang -S quicksort.cc -masm=intel

# --------------------------------------------------------------------------

class GenerateModule (object) :

   def __init__ (self, compiler) :
       super (GenerateModule, self).__init__ ()
       self.compiler = compiler

   def compile (self) :
       if not use_peachpy :
          print ("MISSING peachpy")

       if use_peachpy :
          self.code_program ()

   # -----------------------------------------------------------------------

   def allocate_reg (self) :
       if len (self.registers) != 0 :
          result = self.registers.pop ()
       else :
          result = GeneralPurposeRegister64 ()
       return result

   def free_reg (self, reg) :
       if isinstance (reg, GeneralPurposeRegister) :
          self.registers.insert (0, reg)

   # -----------------------------------------------------------------------

   def code_type (self, t) :
       result = None
       if isinstance (t, SimpleType) :
          if t.type_int :
             # result = int32_t
             result = int64_t
          if t.type_void :
             # result = int32_t # !?
             result = int64_t # !?
       if isinstance (t, PointerType) or isinstance (t, ReferenceType) :
          result = ptr (result)
       return result

   # -----------------------------------------------------------------------

   def jump (self, expr, jump_if_true, lab) :
       if expr.code != None :
          CMP (expr, 0)
          if jump_if_true :
             JNE (lab)
          else :
             JE (lab)
          self.free_reg (expr.code)

   def code_if (self, stat) :
       lab_end = Label ()
       if stat.else_stat != None :
          lab_else = Label ()

       self.code_expr (stat.cond)

       if stat.else_stat == None :
          self.jump (stat.cond, False, lab_end)
       else :
          self.jump (stat.cond, False, lab_else)

       self.code_stat (stat.then_stat)

       if stat.else_stat != None :
          JMP (lab_end)
          LABEL (lab_else)
          self.code_stat (stat.else_stat)

       LABEL (lab_end)

   def code_while (self, stat) :
       """
       lab_begin = Label ()
       lab_end = Label ()
       LABEL (lab_begin)
       self.code_expr (stat.cond)
       JNZ (lab_end)
       self.code_stat (stat.body)
       JMP (lab_begin)
       LABEL (lab_end)
       """

       with Loop () as loop:
          self.code_expr (stat.cond)
          self.jump (stat.cond, False, loop.end)
          self.code_stat (stat.body)
          JMP (loop.begin)


   def code_stat (self, stat) :
       if stat.kind == ifStat :
          with Block () :
             self.code_if (stat)
       elif stat.kind == whileStat :
          self.code_while (stat)
       elif stat.kind == compoundStat :
          with Block () :
             for item in stat.items :
                 self.code_stat (item)
       elif stat.kind == simpleStat :
          self.code_expr (stat.inner_expr)
       elif stat.kind == simpleDecl :
          for item in stat.items :
              self.code_local_variable (item)

   # -----------------------------------------------------------------------

   def code_expr (self, expr) :
       expr.code = None

       if isinstance (expr, CmmSimpleDecl) :
          for item in expr.items :
              self.code_local_variable (item)

       elif isinstance (expr, CmmSimpleStat) :
          pass

       elif expr.kind == simpleName :
          decl = expr.item_decl
          if getattr (decl, "code", None) != None :
             expr.code = self.allocate_reg ()
             if isinstance (decl.code, Argument) :
                LOAD.ARGUMENT (expr.code, decl.code)
             else :
                MOV (expr.code, decl.code)

       elif expr.kind == intValue :
          # expr.code = Constant.uint32 (int (expr.value))
          # expr.code = self.allocate_reg()
          # MOV (expr.code, Constant.uint32 (int (expr.value)))
          expr.code = int (expr.value)

       elif expr.kind == stringValue :
          pass

       elif expr.kind == subexprExpr :
          self.code_expr (expr.param)

       elif expr.kind == indexExpr :
          self.code_expr (expr.left)
          self.code_expr (expr.param)
          if expr.left.code != None and expr.param.code != None :
             # MOV (expr.left.code, [ expr.left.code + 4 * expr.param.code ])
             if isinstance (expr.param.code, GeneralPurposeRegister) :
                MOV (expr.left.code, [ expr.left.code + expr.param.code * 8 ])
             self.free_reg (expr.param.code)

       elif expr.kind == callExpr :
          self.code_expr (expr.left)
          regs = [rcx]
          inx = 0
          for item in expr.param_list.items :
             self.code_expr (item)
             if item.code != None :
                if inx < len (regs) :
                   MOV (regs [inx], item.code)
                else :
                   PUSH (regs [inx], item.code)
             inx = inx + 1
          if expr.left.code != None :
             CALL (expr.left.code)
             self.free_reg (expr.left.code)

       elif expr.kind == assignExpr :
          self.code_expr (expr.right)
          self.code_expr (expr.left)
          if expr.left.code != None and expr.right.code != None :
             MOV (expr.left.code, expr.right.code)
          self.free_reg (expr.right.code)
          self.free_reg (expr.left.code)

       elif expr.kind == logAndExpr or expr.kind == logOrExpr :
          self.code_expr (expr.left)
          lab = Label ()
          if expr.left.code != None  :
             CMP (expr.left.code, 0)
             if expr.kind == logAndExpr :
                JE  (lab)
             else :
                JNE  (lab)
          self.code_expr (expr.right.code)
          if expr.left.code != None and  expr.right.code != None:
             MOV (expr.left.code, expr.right.code)
             self.free_reg (expr.right.code)
          LABEL (lab)
          expr.code = expr.code.left

       elif expr.kind in [ addExpr, subExpr, mulExpr, shlExpr, shrExpr ] :
          self.code_expr (expr.left)
          self.code_expr (expr.right)
          if expr.left.code != None and expr.right.code != None :
             if expr.kind == addExpr :
                # ADD (expr.left.code, expr.right.code)
                ADD (expr.right.code, expr.left.code)
             elif expr.kind == subExpr :
                SUB (expr.left.code, expr.right.code)
             elif expr.kind == mulExpr :
                # IMUL (expr.left.code, expr.right.code)
                IMUL (expr.right.code, expr.right.code, expr.left.code)
             elif expr.kind == shlExpr :
                SHL (expr.left.code, expr.right.code)
             elif expr.kind == shrExpr :
                SHR (expr.left.code, expr.right.code)
          self.free_reg (expr.right.code)
          expr.code = expr.left.code

       elif expr.kind in [ ltExpr, leExpr, gtExpr, geExpr, eqExpr, neExpr ] :
          self.code_expr (expr.left)
          self.code_expr (expr.right)
          if expr.left.code != None and expr.right.code != None :
             CMP (expr.left.code, expr.right.code)
             if expr.kind == ltExpr :
                SETLT (expr.left.code)
             elif expr.kind == leExpr :
                SETLE (expr.left.code)
             elif expr.kind == gtExpr :
                SETGT (expr.left.code)
             elif expr.kind == feExpr :
                SETGE (expr.left.code)
             elif expr.kind == eqExpr :
                SETEQ (expr.left.code)
             elif expr.kind == neExpr :
                SETNE (expr.left.code)
          self.free_reg (expr.right.code)
          expr.code = expr.left.code

   # -----------------------------------------------------------------------

   def code_global_variable (self, decl) :
       # n = 4
       # decl.code = Constant (4*n, 4, (0, ), uint32_t, Name (decl.item_name)) # !?
       # decl.code = Constant (4, 1, (0, ), uint32_t, Name (decl.item_name)) # !?
       decl.code = Constant (8, 1, (0, ), uint64_t, Name (decl.item_name)) # !?

   def code_local_variable (self, decl) :
       decl.code = LocalVariable (8)

   # -----------------------------------------------------------------------

   def code_function (self, decl) :

       result_type = self.code_type (decl.item_type)
       if isinstance (result_type, FunctionType) :
          result_type = result_type.type_from
       if result_type == None :
          result_type = None # !?
       else :
          result_type = self.code_type (result_type)

       params = ( )
       for param in decl.item_list :
           arg = Argument (self.code_type (param.item_type), name = param.item_name)
           param.code = arg
           params = params + ( arg, )

       with Function (decl.item_name, params, result_type) as self.func :
          # decl.code = self.func
          # self.reg = eax # GeneralPurposeRegister32 ()
          # for param in decl.item_list :
          #     LOAD.ARGUMENT (self.reg, param.code)
          self.code_stat (decl.item_body)
          RETURN ()

       # print (self.func.format_instructions ())
       print (self.func.c_signature)
       print (self.func.finalize (self.abi).encode ().format ())

   # -----------------------------------------------------------------------

   def code_program (self) :
       self.abi = abi.detect ()
       # self.abi = linux_x32_abi
       # self.abi = system_v_x86_64_abi
       self.registers = [ ]

       for decl in self.compiler.global_scope.item_list :
           if isinstance (decl, CmmSimpleItem) :
              if decl.is_function :
                 print ("FUNCTION", decl.item_name)
                 self.code_function (decl)
              else :
                 print ("VARIABLE", decl.item_name)
                 self.code_global_variable (decl)

# --------------------------------------------------------------------------

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
