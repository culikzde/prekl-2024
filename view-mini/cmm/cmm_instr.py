
# cmm_instr.py

from __future__ import print_function

import os, sys
# from ctypes import CFUNCTYPE, c_double
import ctypes
from time import time

use_llvmlite = True

try :
   import numpy as np
except :
   print ("missing numpy")
   use_llvmlite = False

try :
   # Fedora 34
   # root: dnf install numpy
   # user: pip install llvmlite
   #       export PYTHONPATH=$HOME/.local/lib/python3.9/site-packages/

   incl_dir = os.path.expanduser ("~/.local/lib/python3.9/site-packages")
   if incl_dir not in sys.path :
      if os.path.exists (incl_dir) :
         sys.path.insert (1, incl_dir)
         print ("ADD", incl_dir)

   work_dir = os.path.abspath (sys.path [0])
   incl_dir = os.path.join (work_dir, "_library/llvmlite-0.40.0rc1")
   if incl_dir not in sys.path :
      if os.path.exists (incl_dir) :
         sys.path.insert (1, incl_dir)
         print ("ADD", incl_dir)

   # import numpy as np
   import llvmlite.binding as llvm
   import llvmlite.ir as ir
except :
   print ("missing llvmlite")
   use_llvmlite = False

# from input import quoteString
# from lexer import LexerInput
# from output import Output
# from code import SimpleType, NamedType
from code import *
from cmm_parser import *

# --------------------------------------------------------------------------

class InstructionsModule (object) :

   def __init__ (self, compiler) :
       super (InstructionsModule, self).__init__ ()
       self.compiler = compiler

   def compile (self) :

       if not use_llvmlite :
          print ("MISSING llvmlite")

       if use_llvmlite :
          # self.example1 ()
          # self.example2 ()
          # self.example3 ()
          self.code_program ()

   # -----------------------------------------------------------------------

   def example1 (self) :
       # llvmlite/examples/ll_fpadd.py

       llvm.initialize ()
       llvm.initialize_native_target ()
       llvm.initialize_native_asmprinter ()

       llvm_ir = """
          ; ModuleID = "examples/ir_fpadd.py"
          target triple = "unknown-unknown-unknown"
          target datalayout = ""

          define double @"fpadd"(double %".1", double %".2")
          {
          entry:
            %"res" = fadd double %".1", %".2"
            ret double %"res"
          }
          """

       # Create a target machine representing the host
       target = llvm.Target.from_default_triple ()
       target_machine = target.create_target_machine ()
       # And an execution engine with an empty backing module
       backing_mod = llvm.parse_assembly ("")
       engine = llvm.create_mcjit_compiler (backing_mod, target_machine)

       # Create a LLVM module object from the IR
       mod = llvm.parse_assembly (llvm_ir)
       mod.verify ()
       # Now add the module and make sure it is ready for execution
       engine.add_module (mod)
       engine.finalize_object ()
       engine.run_static_constructors ()

       # Look up the function pointer (a Python int)
       func_ptr = engine.get_function_address ("fpadd")

       # Run the function via ctypes
       cfunc = ctypes.CFUNCTYPE (ctypes.c_double, ctypes.c_double, ctypes.c_double) (func_ptr)
       res = cfunc (1.0, 3.5)
       print ("RESULT fpadd(...) =", res)

   # -----------------------------------------------------------------------

   def example2 (self) :
       # llvmlite/examples/llvmir.py

       module = ir.Module()
       fntype = ir.FunctionType (ir.IntType (32), [ ir.IntType (32), ir.IntType (32) ])
       func = ir.Function (module, fntype, name="fce")

       bb_entry = func.append_basic_block ()

       builder = ir. IRBuilder()
       builder.position_at_end (bb_entry)

       stackint = builder.alloca (ir.IntType (32))
       builder.store (ir.Constant (stackint.type.pointee, 123), stackint)
       myint = builder.load (stackint)

       addinstr = builder.add (func.args[0], func.args[1])
       mulinstr = builder.mul (addinstr, ir.Constant (ir.IntType(32), 123))
       pred = builder.icmp_signed ('<', addinstr, mulinstr)
       builder.ret (mulinstr)

       bb_block = func.append_basic_block ()
       builder.position_at_end (bb_block)

       bb_exit = func.append_basic_block ()

       pred = builder.trunc (addinstr, ir.IntType(1))
       builder.cbranch (pred, bb_block, bb_exit)

       builder.position_at_end (bb_exit)
       builder.ret (myint)

       print (module)

   # -----------------------------------------------------------------------

   def example3 (self) :
       # llvmlite/examples/sum.py

       llvm.initialize()
       llvm.initialize_native_target()
       llvm.initialize_native_asmprinter()

       t1 = time()

       module = ir.Module ()
       fnty = ir.FunctionType (ir.IntType(32), [ ir.IntType(32).as_pointer(), ir.IntType(32) ])
       func = ir.Function (module, fnty, name="sum")

       bb_entry = func.append_basic_block ()
       bb_loop = func.append_basic_block ()
       bb_exit = func.append_basic_block ()

       builder = ir.IRBuilder ()
       builder.position_at_end (bb_entry)

       builder.branch (bb_loop)
       builder.position_at_end (bb_loop)

       index = builder.phi (ir.IntType(32))
       index.add_incoming (ir.Constant(index.type, 0), bb_entry)
       accum = builder.phi (ir.IntType(32))
       accum.add_incoming (ir.Constant(accum.type, 0), bb_entry)

       ptr = builder.gep (func.args[0], [index])
       value = builder.load (ptr)

       added = builder.add (accum, value)
       accum.add_incoming (added, bb_loop)

       indexp1 = builder.add (index, ir.Constant (index.type, 1))
       index.add_incoming (indexp1, bb_loop)

       cond = builder.icmp_unsigned ('<', indexp1, func.args[1])
       builder.cbranch(cond, bb_loop, bb_exit)

       builder.position_at_end (bb_exit)
       builder.ret(added)

       strmod = str (module)

       t2 = time()

       print("-- generate IR:", t2-t1)

       t3 = time()

       llmod = llvm.parse_assembly (strmod)

       t4 = time()

       print("-- parse assembly:", t4-t3)

       print (llmod)

       pmb = llvm.create_pass_manager_builder ()
       pmb.opt_level = 2
       pm = llvm.create_module_pass_manager ()
       pmb.populate (pm)

       t5 = time ()

       pm.run (llmod)

       t6 = time ()

       print ("-- optimize:", t6-t5)

       t7 = time ()

       target_machine = llvm.Target.from_default_triple().create_target_machine()

       with llvm.create_mcjit_compiler(llmod, target_machine) as ee:
           ee.finalize_object ()
           cfptr = ee.get_function_address ("sum")

           t8 = time ()
           print ("-- JIT compile:", t8 - t7)

           print (target_machine.emit_assembly (llmod))

           cfunc = ctypes.CFUNCTYPE (ctypes.c_int, ctypes.POINTER (ctypes.c_int), ctypes.c_int) (cfptr)
           A = np.arange (10, dtype=np.int32)
           res = cfunc (A.ctypes.data_as (ctypes.POINTER (ctypes.c_int)), A.size)

           print (res, A.sum ())

   # -----------------------------------------------------------------------

   def code_type (self, t) :
       result = None
       if isinstance (t, SimpleType) :
          if t.type_int :
             result = ir.IntType (32)
          if t.type_void :
             result = ir.VoidType ()
       if isinstance (t, ArrayType) :
          cnt = t.lim.value
          if cnt != None :
             elem = self.code_type (t.type_from)
             # result = ir.ArrayType (elem, int (cnt))
             result = ir.VectorType (elem, int (cnt))
       if isinstance (t, PointerType) or isinstance (t, ReferenceType) :
          result = self.code_type (t.type_from).as_pointer ()
       return result

   def code_global_variable (self, decl) :
       t = self.code_type (decl.item_type)
       decl.code = ir.GlobalVariable (self.module, t, decl.item_name)

   def code_local_variable (self, decl) :
       t = self.code_type (decl.item_type)
       if t == None :
          t = ir.IntType (32)
       decl.code = self.builder.alloca (t, name = decl.item_name)

   def code_function (self, decl) :

       result_type = self.code_type (decl.item_type)
       if isinstance (result_type, FunctionType) :
          result_type = result_type.type_from
       if result_type == None :
          result_type = ir.VoidType ()
       else :
          result_type = self.code_type (result_type)

       param_types = [ ]
       for param in decl.item_list :
           param_types.append (self.code_type (param.item_type))

       func_type = ir.FunctionType (result_type, param_types)
       self.func = ir.Function (self.module, func_type, name = decl.item_name)
       decl.code = self.func

       inx = 0
       for param in decl.item_list :
           self.func.args [inx].name = param.item_name
           param.code = self.func.args [inx]
           inx = inx + 1

       entry_block = self.func.append_basic_block ()
       self.builder = ir.IRBuilder ()
       self.builder.position_at_end (entry_block)

       self.code_stat (decl.item_body)
       self.builder.ret_void ()

   # -----------------------------------------------------------------------

   def code_if_stat (self, stat) :
       self.code_expr (stat.cond)
       then_block = self.func.append_basic_block ()
       stop_block = self.func.append_basic_block ()
       if stat.else_stat == None :
          self.builder.cbranch (stat.cond.code, then_block, stop_block)
       else :
          else_block = self.func.append_basic_block ()
          self.builder.cbranch (stat.cond.code, then_block, else_block)

       self.builder.position_at_end (then_block)
       self.code_stat (stat.then_stat)

       if stat.else_stat != None :
          self.builder.position_at_end (else_block)
          self.code_stat (stat.else_stat)
          self.builder.branch (stop_block)

       self.builder.position_at_end (stop_block)

   def code_while_stat (self, stat) :
       start_block = self.func.append_basic_block ()
       cont_block = self.func.append_basic_block ()
       stop_block = self.func.append_basic_block ()

       self.builder.position_at_end (start_block)
       self.code_expr (stat.cond)
       self.builder.cbranch (stat.cond.code, cont_block, stop_block)

       self.builder.position_at_end (cont_block)
       self.code_stat (stat.body)
       self.builder.branch (start_block)

       self.builder.position_at_end (stop_block)

   def code_for_stat (self, stat) :
       self.code_expr (stat.from_expr)
       if stat.iter_expr != None :
          self.code_expr (stat.iter_expr)
          self.code_stat (stat.body)
       else :
          start_block = self.func.append_basic_block ()
          cont_block = self.func.append_basic_block ()
          stop_block = self.func.append_basic_block ()

          self.builder.position_at_end (start_block)
          self.code_expr (stat.cond_expr)
          self.builder.cbranch (stat.cond_expr.code, cont_block, stop_block)

          self.builder.position_at_end (cont_block)
          self.code_stat (stat.body)
          self.code_expr (stat.step_expr)
          self.builder.branch (start_block)

          self.builder.position_at_end (stop_block)

   def code_and_or (self, expr) :
       left = expr.left
       right = expr.right

       self.code_expr (left)

       cont_block = self.func.append_basic_block ()
       stop_block = self.func.append_basic_block ()

       self.builder.position_at_end (cont_block)

       if expr.kind == logAndExpr :
          self.builder.cbranch (left.code, cont_block, stop_block)
       if expr.kind == logOrExpr :
          self.builder.cbranch (left.code, stop_block, cont_block)

       self.builder.position_at_end (cont_block)
       self.code_expr (right)
       # self.builder.branch (stop_block)

       self.builder.position_at_end (stop_block)

   def code_stat (self, stat) :
       if stat.kind == ifStat :
          self.code_if_stat (stat)
       elif stat.kind == whileStat :
          self.code_while_stat (stat)
       elif stat.kind == forStat :
          self.code_for_stat (stat)
       elif stat.kind == compoundStat :
          for item in stat.items :
             self.code_stat (item)
       elif stat.kind == simpleStat :
          self.code_expr (stat.inner_expr)
       elif stat.kind == simpleDecl :
          for item in stat.items :
              self.code_local_variable (item)

   # -----------------------------------------------------------------------

   def code_assign (self, expr) :
       left = expr.left
       right = expr.right
       self.code_expr (right)

       if left.kind != indexExpr :
          self.code_ref (left)
          expr.code = self.builder.store (right.code, left.code)
       else :
          array = left.left
          index = left.param
          self.code_ref (array)
          temp = self.builder.load (array.code)
          self.code_expr (index)
          expr.code = self.builder.insert_element (temp, right.code, index.code)

   def code_ref (self, expr) :
       expr.code = None
       if expr.kind == simpleName :
          if getattr (expr.item_decl, "code", None) == None  :
             expr.code = self.builder.alloca (ir.IntType (32), name = "unknown")
          else :
             expr.code = expr.item_decl.code

   def code_expr (self, expr) :
       expr.code = None

       # if expr.kind == simpleDecl :
       #    expr.code = self.builder.load (expr.item_decl.code)

       if expr.kind == simpleStat :
          self.code_stat (expr)
          expr.code = expr.inner_expr.code

       elif expr.kind == simpleName :
          code = getattr (expr.item_decl, "code", None)
          if code == None :
             expr.code = self.builder.alloca (ir.IntType (32), name = "unknown")
          else :
             if isinstance (code.type, ir.types.PointerType):
                expr.code = self.builder.load (code)
             else :
                expr.code = code

       elif expr.kind == intValue :
          expr.code = ir.Constant (ir.IntType (32), str (expr.value))

       elif expr.kind == stringValue :
          pass

       elif expr.kind == subexprExpr :
          self.code_expr (expr.param)

       elif expr.kind == assignExpr :
          self.code_assign (expr)

       elif expr.kind == indexExpr :
          left = expr.left
          right = expr.param
          self.code_ref (left)
          temp = self.builder.load (left.code)
          self.code_expr (right)
          expr.code = self.builder.extract_element (temp, right.code)

       elif expr.kind == callExpr :
          left = expr.left
          args = ()
          for item in expr.param_list.items :
              self.code_expr (item)
              args = args + ( item.code , )
          self.code_ref (left)
          temp = self.builder.load (left.code)
          expr.code = self.builder.call (temp, args)

       elif expr.kind == logAndExpr or expr.kind == logOrExpr :
          self.code_and_or (expr)

       elif expr.kind in [ addExpr, subExpr, mulExpr, divExpr, modExpr,
                           shlExpr, shrExpr,
                           bitAndExpr, bitOrExpr, bitXorExpr ] :
          left = expr.left
          right = expr.right
          self.code_expr (left)
          self.code_expr (right)
          if expr.kind == addExpr :
             expr.code = self.builder.add (left.code, right.code)
          elif expr.kind == subExpr :
             expr.code = self.builder.sub (left.code, right.code)
          elif expr.kind == mulExpr :
             expr.code = self.builder.mul (left.code, right.code, name = "multiply")
          elif expr.kind == divExpr :
             expr.code = self.builder.sdiv (left.code, right.code)
          elif expr.kind == modExpr :
             expr.code = self.builder.srem (left.code, right.code)
          elif expr.kind == shlExpr :
             expr.code = self.builder.shl (left.code, right.code)
          elif expr.kind == shrExpr :
             expr.code = self.builder.ashr (left.code, right.code)
          elif expr.kind == bitAndExpr :
             expr.code = self.builder.and_ (left.code, right.code)
          elif expr.kind == bitOrExpr :
             expr.code = self.builder.or_ (left.code, right.code)
          elif expr.kind == bitXorExpr :
             expr.code = self.builder.xor (left.code, right.code)

       elif expr.kind in [ ltExpr, leExpr, gtExpr, geExpr, eqExpr, neExpr ] :
          left = expr.left
          right = expr.right
          self.code_expr (left)
          self.code_expr (right)
          if expr.kind == ltExpr :
             cmp = "<"
          elif expr.kind == leExpr :
             cmp = "<="
          elif expr.kind == leExpr :
             cmp = "<="
          elif expr.kind == gtExpr :
             cmp = ">"
          elif expr.kind == geExpr :
             cmp = ">="
          elif expr.kind == eqExpr :
             cmp = "=="
          elif expr.kind == neExpr :
             cmp = "!="
          expr.code = self.builder.icmp_signed (cmp, left.code, right.code)

       elif expr.kind == minusExpr :
          param = expr.param
          self.code_expr (param)
          expr.code = self.builder.neg (param.code)

       elif expr.kind == bitNotExpr :
          param = expr.param
          self.code_expr (param)
          expr.code = self.builder.not_ (param.code)

       if expr.code == None :
          expr.code = ir.Constant (ir.IntType (32), 42) # !?

   # -----------------------------------------------------------------------

   def code_program (self) :
       llvm.initialize ()
       llvm.initialize_native_target ()
       llvm.initialize_native_asmprinter ()
       llvm.initialize_all_targets ()
       llvm.initialize_all_asmprinters ()

       self.module = ir.Module ()

       for decl in self.compiler.global_scope.item_list :
           if isinstance (decl, CmmSimpleItem) :
              if decl.is_function :
                 print ("FUNCTION", decl.item_name)
                 self.code_function (decl)
              else :
                 print ("VARIABLE", decl.item_name)
                 self.code_global_variable (decl)

       print (self.module)

       strmod = str (self.module)
       # print ("SOURCE")
       # print (strmod)
       llmod = llvm.parse_assembly (strmod)
       # print ("ASSEMBLY")
       # print (llmod)

       pmb = llvm.create_pass_manager_builder ()
       pmb.opt_level = 2
       pm = llvm.create_module_pass_manager ()
       pmb.populate (pm)
       pm.run (llmod)
       # print ("OPTIMIZED")
       # print (llmod)

       target = llvm.Target.from_default_triple ()
       # target = llvm.Target.from_triple ("i386-pc-linux-gnu")
       # target = llvm.Target.from_triple ("x86_64-pc-linux-gnu")

       # target = llvm.Target.from_triple ("x86_64-w64-windows-gnu")
       # target = llvm.Target.from_triple ("i686-pc-windows-gnu")
       # target = llvm.Target.from_triple ("x86_64-pc-windows-gnu")

       # arm : initialize_all_targets, initialize_all_asmprinters
       # target = llvm.Target.from_triple ("arm-none-eabi")
       # target = llvm.Target.from_triple ("arm64-unknown-linux")

       # target = llvm.Target.from_triple ("riscv32-unknown-linux")
       # target = llvm.Target.from_triple ("riscv64-unknown-linux")

       # target = llvm.Target.from_triple ("wasm32-unknown-unknown")
       # target = llvm.Target.from_triple ("wasm64-unknown-unknown")

       machine = target.create_target_machine ()
       with llvm.create_mcjit_compiler (llmod, machine) as ee:
           ee.finalize_object ()
           print (machine.emit_assembly (llmod))

           obj = machine.emit_object (llmod)
           fileName = self.compiler.win.outputFileName ("instr.o")
           f = open (fileName, "wb")
           f.write (obj)
           f.close()

       # see llvmlite/tests/test_binding.py

# --------------------------------------------------------------------------

# llc instr.ll -x86-asm-syntax=intel -mtriple=i386-pc-linux-gnu
# lli

# https://stackoverflow.com/questions/15036909/clang-how-to-list-supported-target-architectures
# clang -print-targets

# i386-pc-linux-gnu
# x86_64-pc-linux-gnu

# x86_64-w64-windows-gnu # same as x86_64-w64-mingw32
# i686-pc-windows-gnu # MSVC
# x86_64-pc-windows-gnu # MSVC 64-BIT

# arm-none-eabi
# arm64-unknown-linux

# riscv32-unknown-linux
# riscv64-unknown-linux

# nvptx-unknown-unknown
# nvptx64-unknown-unknown

# wasm32-unknown-unknown
# wasm64-unknown-unknown

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
