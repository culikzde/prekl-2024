#!/bin/env python

import sys

use_qt6 = False
use_qt5 = False
use_qt4 = False

try :
   from PyQt5.QtCore import *
   from PyQt5.QtGui import *
   from PyQt5.QtWidgets import *
   use_qt5 = True
except :
   try :
      from PyQt6.QtCore import *
      from PyQt6.QtGui import *
      from PyQt6.QtWidgets import *
      use_qt6 = True
   except :
      from PyQt4.QtCore import *
      from PyQt4.QtGui import *
      use_qt4 = True

app = QApplication (sys.argv)

# import faulthandler
# faulthandler.enable ()

import os
if use_qt4 :
   opts = os.popen ("pkg-config QtCore QtGui --cflags").read().split ()
if use_qt5 :
   opts = os.popen ("pkg-config Qt5Core Qt5Gui Qt5Widgets --cflags").read().split ()
if use_qt6 :
   opts = os.popen ("pkg-config Qt6Core Qt6Gui Qt6Widgets --cflags").read().split ()
   opts = [ "--std=c++17" ] + opts + [ "-lstdc++" ]

import interpreter
comp = interpreter.Interpreter ()

# opts = opts + [ "-g" ] # line numbers
opts = opts + [ "-fPIC" ]
opts = opts + [ "sample-qt.cc" ]

if use_qt4 :
    libs = [
            "/usr/lib64/libstdc++.so.6",
            "/usr/lib64/libQtCore.so",
            "/usr/lib64/libQtGui.so",
          ]
else :
    libs = [
            "/usr/lib/x86_64-linux-gnu/libstdc++.so.6",
            "/usr/lib/x86_64-linux-gnu/libQt5Core.so",
            "/usr/lib/x86_64-linux-gnu/libQt5Gui.so",
            "/usr/lib/x86_64-linux-gnu/libQt5Widgets.so",
          ]
          
    libs = [
            "/usr/lib64/libstdc++.so.6",
            "/usr/lib64/libQt5Core.so",
            "/usr/lib64/libQt5Gui.so",
            "/usr/lib64/libQt5Widgets.so",
          ]

comp.compileAndRun (opts, libs, ["sample-qt"], [])
