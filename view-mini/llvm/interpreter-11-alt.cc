// //===-- examples/clang-interpreter/main.cpp - Clang C Interpreter Example -===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//

#include "clang/Basic/DiagnosticOptions.h"
#include "clang/CodeGen/CodeGenAction.h"
#include "clang/Driver/Compilation.h"
#include "clang/Driver/Driver.h"
#include "clang/Driver/Tool.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/CompilerInvocation.h"
#include "clang/Frontend/FrontendDiagnostic.h"
#include "clang/Frontend/TextDiagnosticPrinter.h"
#include "llvm/ADT/SmallString.h"
#include "llvm/ExecutionEngine/ExecutionEngine.h"
#include "llvm/ExecutionEngine/Orc/CompileUtils.h"
#include "llvm/ExecutionEngine/Orc/ExecutionUtils.h"
#include "llvm/ExecutionEngine/Orc/IRCompileLayer.h"
#include "llvm/ExecutionEngine/Orc/RTDyldObjectLinkingLayer.h"
#include "llvm/ExecutionEngine/SectionMemoryManager.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/IR/Mangler.h"
#include "llvm/IR/Module.h"
#include "llvm/Support/FileSystem.h"
#include "llvm/Support/Host.h"
#include "llvm/Support/ManagedStatic.h"
#include "llvm/Support/Path.h"
#include "llvm/Support/TargetSelect.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Target/TargetMachine.h"

using namespace clang;
using namespace clang::driver;

using namespace llvm;
using namespace llvm::orc;

#if LLVM_VERSION_MAJOR == 9
   #include "dlfcn.h"
   using llvm::make_unique;
#else
   using std::make_unique;
#endif

// This function isn't referenced outside its translation unit, but it
// can't use the "static" keyword because its address is used for
// GetMainExecutable (since some platforms don't support taking the
// address of main, and some platforms can't implement GetMainExecutable
// without being given the address of a function in the main executable).
std::string GetExecutablePath(const char *Argv0, void *MainAddr) {
  return llvm::sys::fs::getMainExecutable(Argv0, MainAddr);
}

/* ---------------------------------------------------------------------- */

std::unique_ptr<SectionMemoryManager> createMemMgr() {
    return make_unique<SectionMemoryManager>();
}

/* ---------------------------------------------------------------------- */

#if 0
namespace llvm {
namespace orc {

class SimpleJIT {
private:
  ExecutionSession ES;
  std::unique_ptr<TargetMachine> TM;
  const DataLayout DL;
  MangleAndInterner Mangle{ES, DL};
  #if LLVM_VERSION_MAJOR == 9
  #elif LLVM_VERSION_MAJOR == 10
  JITDylib &MainJD{ES.createJITDylib("<main>")};
  #else
  JITDylib &MainJD{ES.createBareJITDylib("<main>")};
  #endif
  RTDyldObjectLinkingLayer ObjectLayer{ES, createMemMgr};
  #if LLVM_VERSION_MAJOR == 9
  IRCompileLayer CompileLayer{ES, ObjectLayer, SimpleCompiler(*TM)};
  #else
  IRCompileLayer CompileLayer{ES, ObjectLayer,
                              make_unique<SimpleCompiler>(*TM)};
  #endif

  static std::unique_ptr<SectionMemoryManager> createMemMgr() {
    return make_unique<SectionMemoryManager>();
  }

  SimpleJIT(
      std::unique_ptr<TargetMachine> TM, DataLayout DL,
      #if LLVM_VERSION_MAJOR == 9
      DynamicLibrarySearchGenerator ProcessSymbolsGenerator
      #else
      std::unique_ptr<DynamicLibrarySearchGenerator> ProcessSymbolsGenerator
      #endif
      )
      :
      #if LLVM_VERSION_MAJOR > 12
         ES(cantFail(SelfExecutorProcessControl::Create())),
      #endif
    TM(std::move(TM)), DL(std::move(DL)) {
    llvm::sys::DynamicLibrary::LoadLibraryPermanently(nullptr);
    #if LLVM_VERSION_MAJOR == 9
    ES.getMainJITDylib().setGenerator(std::move(ProcessSymbolsGenerator));
    #else
    MainJD.addGenerator(std::move(ProcessSymbolsGenerator));
    #endif

    /*
    for (string path : add_libraries)
    {
       auto g = DynamicLibrarySearchGenerator::Load (path.c_str(), DL.getGlobalPrefix ());
       MainJD.addGenerator(std::move (*g));
    }

    auto g0 = DynamicLibrarySearchGenerator::Load ("/usr/lib64/libstdc++.so.6", DL.getGlobalPrefix ());
    MainJD.addGenerator(std::move(*g0));

    auto g1 = DynamicLibrarySearchGenerator::Load ("/usr/lib64/libQt5Core.so", DL.getGlobalPrefix ());
    MainJD.addGenerator(std::move(*g1));

    auto g2 = DynamicLibrarySearchGenerator::Load ("/usr/lib64/libQt5Gui.so", DL.getGlobalPrefix ());
    MainJD.addGenerator(std::move(*g2));

    auto g3 = DynamicLibrarySearchGenerator::Load ("/usr/lib64/libQt5Widgets.so", DL.getGlobalPrefix ());
    MainJD.addGenerator(std::move(*g3));
    */
  }

public:
  #if LLVM_VERSION_MAJOR >= 12
  ~SimpleJIT() {
    if (auto Err = ES.endSession())
      ES.reportError(std::move(Err));
  }
  #endif

  static Expected<std::unique_ptr<SimpleJIT>> Create() {
    auto JTMB = JITTargetMachineBuilder::detectHost();
    if (!JTMB)
      return JTMB.takeError();

    auto TM = JTMB->createTargetMachine();
    if (!TM)
      return TM.takeError();

    auto DL = (*TM)->createDataLayout();

    auto ProcessSymbolsGenerator =
        DynamicLibrarySearchGenerator::GetForCurrentProcess(
            DL.getGlobalPrefix());

    if (!ProcessSymbolsGenerator)
      return ProcessSymbolsGenerator.takeError();

    return std::unique_ptr<SimpleJIT>(new SimpleJIT(
        std::move(*TM), std::move(DL), std::move(*ProcessSymbolsGenerator)));
  }

  const TargetMachine &getTargetMachine() const { return *TM; }

  Error addModule(ThreadSafeModule M) {
    #if LLVM_VERSION_MAJOR == 9
    return CompileLayer.add(ES.getMainJITDylib(), std::move(M));
    #else
    return CompileLayer.add(MainJD, std::move(M));
    #endif
  }

  Expected<JITEvaluatedSymbol> findSymbol(const StringRef &Name) {
     #if LLVM_VERSION_MAJOR == 9
     return ES.lookup({&ES.getMainJITDylib()}, Mangle(Name));
     #else
    return ES.lookup({&MainJD}, Mangle(Name));
     #endif
  }

  Expected<JITTargetAddress> getSymbolAddress(const StringRef &Name) {
     auto Sym = findSymbol(Name);
     if (!Sym)
        return Sym.takeError();
     return Sym->getAddress();
  }

  void addLibrary (std::string lib_path)
  {
     #if LLVM_VERSION_MAJOR == 9
         // not implemented
        void* lib_handle = dlopen (lib_path.c_str(),  RTLD_NOW | RTLD_GLOBAL);
        if (lib_handle == NULL) 
           llvm::errs() << "library not loaded: " << lib_path << "\n";

     #else
        auto g = DynamicLibrarySearchGenerator::Load (lib_path.c_str(), DL.getGlobalPrefix ());
        MainJD.addGenerator(std::move (*g));
     #endif
  }
};

} // end namespace orc
} // end namespace llvm
#endif

/* ---------------------------------------------------------------------- */

int execute (const char * name,
             std::vector <const char *> options,
             std::vector <std::string> libraries,
             int new_argc,
             const char * * new_argv,
             char * const * new_envp)
{
  int Res = 255;

  // This just needs to be some symbol in the binary; C++ doesn't
  // allow taking the address of ::main however.
  void *MainAddr = (void*) (intptr_t) GetExecutablePath;
  std::string Path = GetExecutablePath(name, MainAddr);
  IntrusiveRefCntPtr<DiagnosticOptions> DiagOpts = new DiagnosticOptions();
  TextDiagnosticPrinter *DiagClient =
    new TextDiagnosticPrinter(llvm::errs(), &*DiagOpts);

  IntrusiveRefCntPtr<DiagnosticIDs> DiagID(new DiagnosticIDs());
  DiagnosticsEngine Diags(DiagID, &*DiagOpts, DiagClient);

  const std::string TripleStr = llvm::sys::getProcessTriple();
  llvm::Triple T(TripleStr);

  // Use ELF on Windows-32 and MingW for now.
#ifndef CLANG_INTERPRETER_COFF_FORMAT
  if (T.isOSBinFormatCOFF())
    T.setObjectFormat(llvm::Triple::ELF);
#endif

  Driver TheDriver(Path, T.str(), Diags);
  TheDriver.setTitle("clang interpreter");
  TheDriver.setCheckInputsExist(false);

  // FIXME: This is a hack to try to force the driver to do something we can
  // recognize. We need to extend the driver library to support this use model
  // (basically, exactly one input, and the operation mode is hard wired).
  SmallVector<const char *, 16> Args;
  Args.push_back (name);
  for (const char * option : options)
  {
      Args.push_back (option);
      // std::cout << "ARG " << option << std::endl;
  }
  Args.push_back("-fsyntax-only");

  std::unique_ptr<Compilation> C(TheDriver.BuildCompilation(Args));
  if (!C)
    return Res;

  // FIXME: This is copied from ASTUnit.cpp; simplify and eliminate.

  // We expect to get back exactly one command job, if we didn't something
  // failed. Extract that job from the compilation.
  const driver::JobList &Jobs = C->getJobs();
  if (Jobs.size() != 1 || !isa<driver::Command>(*Jobs.begin())) {
    SmallString<256> Msg;
    llvm::raw_svector_ostream OS(Msg);
    Jobs.Print(OS, "; ", true);
    Diags.Report(diag::err_fe_expected_compiler_job) << OS.str();
    return Res;
  }

  const driver::Command &Cmd = cast<driver::Command>(*Jobs.begin());
  if (llvm::StringRef(Cmd.getCreator().getName()) != "clang") {
    Diags.Report(diag::err_fe_expected_clang_command);
    return Res;
  }

  // Initialize a compiler invocation object from the clang (-cc1) arguments.
  const llvm::opt::ArgStringList &CCArgs = Cmd.getArguments();
  std::unique_ptr<CompilerInvocation> CI(new CompilerInvocation);
  #if LLVM_VERSION_MAJOR == 9
  CompilerInvocation::CreateFromArgs(*CI,
                                     const_cast<const char **>(CCArgs.data()),
                                     const_cast<const char **>(CCArgs.data()) +
                                       CCArgs.size(),
                                     Diags);
  #else
  CompilerInvocation::CreateFromArgs(*CI, CCArgs, Diags);
  #endif

  // Show the invocation, with -v.
  if (CI->getHeaderSearchOpts().Verbose) {
    llvm::errs() << "clang invocation:\n";
    Jobs.Print(llvm::errs(), "\n", true);
    llvm::errs() << "\n";
  }

  // FIXME: This is copied from cc1_main.cpp; simplify and eliminate.

  // Create a compiler instance to handle the actual work.
  CompilerInstance Clang;
  Clang.setInvocation(std::move(CI));

  // Create the compilers actual diagnostics engine.
  Clang.createDiagnostics();
  if (!Clang.hasDiagnostics())
    return Res;

  // Infer the builtin include path if unspecified.
  if (Clang.getHeaderSearchOpts().UseBuiltinIncludes &&
      Clang.getHeaderSearchOpts().ResourceDir.empty())
    Clang.getHeaderSearchOpts().ResourceDir =
      CompilerInvocation::GetResourcesPath(name, MainAddr);

  // Create and execute the frontend to generate an LLVM bitcode module.
  std::unique_ptr<CodeGenAction> Act(new EmitLLVMOnlyAction());
  if (!Clang.ExecuteAction(*Act))
    return Res;

  llvm::InitializeNativeTarget();
  llvm::InitializeNativeTargetAsmParser(); // added
  llvm::InitializeNativeTargetAsmPrinter();

  std::unique_ptr<llvm::LLVMContext> Ctx(Act->takeLLVMContext());
  std::unique_ptr<llvm::Module> Module = Act->takeModule();

  if (Module) {
    // auto J = ExitOnErr(llvm::orc::SimpleJIT::Create());

    // auto JTMB = JITTargetMachineBuilder::detectHost();
    Expected<JITTargetMachineBuilder> JTMB = JITTargetMachineBuilder::detectHost();
    if (!JTMB)
       return Res;

    // auto TME = JTMB->createTargetMachine();
    Expected<std::unique_ptr<TargetMachine>> TME = JTMB->createTargetMachine();
    if (!TME)
      return Res;

    std::unique_ptr<TargetMachine> TM = std::move (*TME);

    // auto DL = (*TME)->createDataLayout();
    const DataLayout DL = TM->createDataLayout();

    // auto ProcessSymbolsGenerator =
    #if LLVM_VERSION_MAJOR == 9
    Expected<DynamicLibrarySearchGenerator> ProcessSymbolsGenerator =
        DynamicLibrarySearchGenerator::GetForCurrentProcess(DL.getGlobalPrefix());
    #else
    Expected<std::unique_ptr<DynamicLibrarySearchGenerator>> ProcessSymbolsGenerator =
        DynamicLibrarySearchGenerator::GetForCurrentProcess(DL.getGlobalPrefix());
    #endif

    if (!ProcessSymbolsGenerator)
      return Res;

    // return std::unique_ptr<SimpleJIT>(new SimpleJIT(
    //     std::move(*TM), std::move(DL), std::move(*ProcessSymbolsGenerator)));

    #if LLVM_VERSION_MAJOR > 12
        ExecutionSession ES(cantFail(SelfExecutorProcessControl::Create()));
    #else
       ExecutionSession ES;
    #endif

    // const DataLayout DL;
    MangleAndInterner Mangle{ES, DL};

    #if LLVM_VERSION_MAJOR == 9
    #elif LLVM_VERSION_MAJOR == 10
    JITDylib &MainJD{ES.createJITDylib("<main>")};
    #else
    JITDylib &MainJD{ES.createBareJITDylib("<main>")};
    #endif

    RTDyldObjectLinkingLayer ObjectLayer{ES, createMemMgr};
    #if LLVM_VERSION_MAJOR == 9
    IRCompileLayer CompileLayer{ES, ObjectLayer, SimpleCompiler(*TM)};
    #else
    IRCompileLayer CompileLayer{ES, ObjectLayer,
                                make_unique<SimpleCompiler>(*TM)};
    #endif

    // TM(std::move(TM)), DL(std::move(DL))
    llvm::sys::DynamicLibrary::LoadLibraryPermanently(nullptr);
    #if LLVM_VERSION_MAJOR == 9
    ES.getMainJITDylib().setGenerator(std::move(*ProcessSymbolsGenerator));
    #else
    MainJD.addGenerator(std::move(*ProcessSymbolsGenerator));
    #endif

    // for (std::string library : libraries)
    //     J->addLibrary (library);
    for (std::string library : libraries)
    {
        #if LLVM_VERSION_MAJOR == 9
            // not implemented
            void* lib_handle = dlopen (library.c_str(),  RTLD_NOW | RTLD_GLOBAL);
            if (lib_handle == NULL)
              llvm::errs() << "library not loaded: " << library << "\n";

        #else
            auto g = DynamicLibrarySearchGenerator::Load (library.c_str(), DL.getGlobalPrefix ());
            MainJD.addGenerator(std::move (*g));
        #endif
    }

    // ExitOnErr(J->addModule(
    //     llvm::orc::ThreadSafeModule(std::move(Module), std::move(Ctx))));
    #if LLVM_VERSION_MAJOR == 9
    CompileLayer.add(ES.getMainJITDylib(), llvm::orc::ThreadSafeModule(std::move(Module), std::move(Ctx)));
    #else
    CompileLayer.add(MainJD, llvm::orc::ThreadSafeModule(std::move(Module), std::move(Ctx)));
    #endif

    // auto Main = (int (*)(...))ExitOnErr(J->getSymbolAddress("main"));
    const StringRef Name = "main";
    Expected<JITEvaluatedSymbol> Sym =
     #if LLVM_VERSION_MAJOR == 9
         ES.lookup({&ES.getMainJITDylib()}, Mangle(Name));
     #else
         ES.lookup({&MainJD}, Mangle(Name));
     #endif
     if (!Sym)
        return Res;

     int k = 0;
     while (new_envp [k] != NULL)
     {
         std::cout << "ENV " << k << " ... " <<  new_envp [k] << std::endl;
         k ++;
     }

     auto Main = (int (*)(...)) Sym->getAddress();

     Res = Main (new_argc, new_argv, new_envp);
  }

  // Shutdown.
  llvm::llvm_shutdown();

  return Res;
}
