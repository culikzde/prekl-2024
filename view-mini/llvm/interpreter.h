
/* interpreter.h */

#ifndef INTEPRETER_H
#define INTEPRETER_H

#include <string>
#include <vector>
// using namespace std;

class Interpreter
{
   public:
      Interpreter () { }
      int compileAndRun (std::vector <std::string> options,
                         std::vector <std::string> libraries,
                         std::vector <std::string> new_argumets,
                         std::vector <std::string> new_environment);
};

#endif
