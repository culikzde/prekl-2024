
/* interpreter-main.cc */

#include "interpreter.cc"

// #include <sys/types.h>
#include <sys/stat.h> // struct stat

/* ---------------------------------------------------------------------- */

int main (int argc, const char * * argv)
{
    std::vector <std::string> options;
    std::vector <std::string> libraries;

    struct stat info;
    std::string lib_dir = "/usr/lib/x86_64-linux-gnu/"; // Debian
    if (stat (lib_dir.c_str(), &info) != 0)
    {
        lib_dir = "/usr/lib64/"; // Fedora
        if (stat (lib_dir.c_str(), &info) != 0)
        {
           lib_dir = "/usr/lib/";
        }
    }

    for (int i = 1; i < argc; i ++)
    {
        std::string arg = argv [i];
        if (arg.length () < 2 || arg.substr (0, 2) != "-l")
        {
           // std::cout << "OPTION " << arg << std::endl;
           options.push_back (arg);
        }
        else
        {
           arg = arg.substr (2);
           // std::cout << "LIBRARY " << arg << std::endl;
           std::string lib = "lib" + arg + ".so";

           // if (arg == "stdc++")
           //    lib = lib + ".6";
           // if (arg == "QtCore" || arg == "QtGui")
           //    lib = lib + ".4.8.7";

           lib = lib_dir + lib; 

           // std::cout << "library " << lib << std::endl;
           libraries.push_back (lib);
        }
    }

    std::vector <std::string> new_arguments; //  = { "appl_name", "some text" };
    std::vector <std::string> new_environment; //  = { "PATH=/bin", "ABC=DEF" };

    Interpreter comp;
    comp.compileAndRun (options, libraries, new_arguments, new_environment);
}

/* ---------------------------------------------------------------------- */
