
#include <gtk/gtk.h>

int main (int argc, char * * argv)
{
   gtk_init (&argc, &argv);

   GtkWidget *window;
   GtkWidget *button;

   window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
   button = gtk_button_new_with_label ("Hello World");

   gtk_container_add (GTK_CONTAINER (window), button);

   g_signal_connect (window, "destroy", gtk_main_quit, NULL);
   g_signal_connect (button, "clicked", gtk_main_quit, NULL);

   gtk_widget_show_all (window);

   gtk_main ();
}
