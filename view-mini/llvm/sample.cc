#include <stdio.h> // printf
#include <stdlib.h> // system
#include <unistd.h> // environ;

int main (int argc, const char** argv)
{
   printf ("Hello\n");
   int i = 1;
   i ++;
   printf ("Sample i = %d \n", i);

   printf ("argc = %d \n", argc);
   for (int k = 0; k < argc; k++)
       printf ("arg %d = %s \n", k, argv [k]);

   /*
   int k = 0;
   while (environ [k] != NULL)
   {
       printf ("env %d = %s \n", k, environ [k]);
       k ++;
   }
   */

   // system ("/usr/bin/printenv");

   return 0;
}
