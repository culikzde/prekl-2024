test -f Makefile && make clean
rm Makefile
rm interpreter.sbf
rm interpreter.exp
rm sipAPIinterpreter.h
rm sipinterpretercmodule.cpp
rm sipinterpretercmodule.o
rm sipinterpreterInterpreter.cpp
rm sipinterpreterInterpreter.o
rm sipinterpreterstdstring.cpp
rm sipinterpreterstdstring.o
rm sipinterpreterstdvector0100stdstring.cpp
rm sipinterpreterstdvector0100stdstring.o
rm interpreter.so

rm interpreter-main.o
rm interpreter-main.dwo
rm interpreter.bin

rm _build/sample-qt.gch
rm -rf _build
