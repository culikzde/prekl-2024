#!/bin/env python

import PyQt5.QtCore
import PyQt5.QtGui
import PyQt5.QtWidgets

from PyQt5 import QtCore
from PyQt5 import QtGui
from PyQt5 import QtWidgets

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

import sys, importlib

importlib.import_module ("PyQt5.QtCore")
importlib.import_module ("PyQt5.QtGui")
importlib.import_module ("PyQt5.QtWidgets")

import highlight

class Window (QTextEdit) :

   def __init__ (self, parent = None) :
       super (Window, self).__init__ (parent)
       self.highlighter = highlight.Highlighter (self.document ()) # important: keep reference to highlighter

       text = """
          int main (int argc, char * * argv)
          {
             QString s = "abc";
             return 0;
          }
       """
       self.setText (text)

app = QApplication (sys.argv)
win = Window ()

win.show ()
app.exec_ ()
