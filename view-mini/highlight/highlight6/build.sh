#!/bin/sh

sip-build --verbose --target-dir=. --no-make # --qmake=/usr/bin/qmake6

cd _build && make && cd .. || exit 1
cp _build/highlight/libhighlight.so highlight.so || exit 1

python run.py

# pacman -S sip (python-pyqt6-sip) python-pyqt6 pyqt-builder
# conflict with sip4 python-sip4

